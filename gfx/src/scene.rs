//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

//! Camera, lights, and other commonly used data in shaders.

use gl::types::*;
use glam::{const_vec3, Vec3};

/// View angle used in scenes, in radians.
pub const VIEW_ANGLE: f32 = std::f32::consts::FRAC_PI_8;

/// A camera that defines a look-at matrix.
///
/// Enforces the following conventions:
///
///     - the up direction in world-space is the +y direction,
///
///     - the look-at transformation is right-handed.
pub struct Camera {
	pub eye: Vec3,
	pub center: Vec3,
}

impl Camera {
	const UP: Vec3 = const_vec3!([0.0, 1.0, 0.0]);

	/// Look-at matrix of the camera.
	pub fn look_at(&self) -> glam::Mat4 {
		glam::Mat4::look_at_rh(self.eye, self.center, Camera::UP)
	}
}

/// Projection matrix to clip-space.
pub struct Projection {
	pub mat: glam::Mat4,
}

impl Projection {
	/// Create a projection from a 4×4 matrix.
	///
	/// Does not assume anything about the matrix. Use the
	/// ['glam::Mat4']::perspective and ['glam::Mat4']::orthographic family
	/// of functions.
	// Pretty lax. Instead, I could enforce the right-handed, [-1, 1]
	// depth convention through this type. Maybe the z_range too,
	// but that'd require me to think about it some more.
	pub fn new(mat: glam::Mat4) -> Projection {
		Projection { mat }
	}
}

/// Point light.
///
/// The 4th components in `pos` and `color` are padding ; 3 components
/// vectors are a pain to handle correctly in OpenGL shaders.
#[repr(C)]
pub struct PointLight {
	pub pos: glam::Vec4,
	pub color: glam::Vec4,
}

/// Default: a point at origin radiating no light.
impl Default for PointLight {
	fn default() -> PointLight {
		PointLight {
			pos: glam::Vec4::new(0.0, 0.0, 0.0, 0.0),
			color: glam::Vec4::new(0.0, 0.0, 0.0, 0.0),
		}
	}
}

#[repr(C)]
pub struct Outline(pub GLfloat);
