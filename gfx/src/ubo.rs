//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

// TODO: doc

pub unsafe trait UboGet<U: Ubo> {
	const BINDING: gl::types::GLuint;
	const NAME: *const gl::types::GLchar;
	fn get_mut_of(ubo: &mut U) -> &mut Self;
}

pub unsafe trait Ubo {
	/// Send all data to the OpenGL buffer.
	///
	/// # Safety
	/// ['uniform_buffer'] will implement this. It'll assume the UBO
	/// is already bound.
	unsafe fn buffer_data(&self);

	/// Returns the `GLuint` representing the buffer in the GL api.
	///
	/// # Safety
	/// Let ['uniform_buffer'] implement this.
	unsafe fn gl_name(&self) -> gl::types::GLuint;
}

pub type MaterialUboKeyBits = u32;
// No ties to a particular MaterialUbo type, instance or lifetime.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct MaterialUboKey(pub MaterialUboKeyBits);

impl MaterialUboKey {
	pub const NO_MATERIAL: MaterialUboKey = MaterialUboKey(MaterialUboKeyBits::MAX);
}

pub trait MaterialUbo {
	type StoreIdx: Clone + Copy;
	fn gl_name(&self) -> gl::types::GLuint;
	fn key_for(&self, indices: Self::StoreIdx) -> MaterialUboKey;
	/// Bind the materials referenced by a key.
	///
	/// # Safety
	///
	/// Assumes the key is valid for the material store.
	unsafe fn bind_key(&self, key: MaterialUboKey);
}

pub trait MaterialUboField<U: MaterialUbo> {
	const FIELD_NUMBER: usize;
	const BIT_OFFSET: MaterialUboKeyBits;
	const BIT_LEN: MaterialUboKeyBits;
	const STORE_MASK: MaterialUboKeyBits;
	const BINDING: gl::types::GLuint;
	const NAME: *const gl::types::GLchar;
	/// Send itself to the GL UBO.
	///
	/// # Safety
	///
	/// Assumes the UBO is bound.
	unsafe fn buffer_subdata(&self, ubo: &U);
}

#[macro_export]
macro_rules! uniform_buffers {
	(mod $modname:ident {
		pub struct $scnname:ident {
			$($scnfield:ident : $scntype:path),+ $(,)?
		}
		pub struct $matname:ident {
			$($matfield:ident : [$mattype:path; $matcount:literal]),+ $(,)?
		}
	}) => { mod $modname {
    		use super::*;
		uniform_buffers!(@scnstruct, $scnname, $($scnfield : $scntype),+);
		uniform_buffers!(@scnimpl, $scnname, $($scnfield : $scntype),+);
		uniform_buffers!(@scnubo, $scnname);
		uniform_buffers!(@scnuboget, $scnname, 0, $($scnfield : $scntype),+);
		uniform_buffers!(@scndrop, $scnname);

		uniform_buffers!(@matstruct, $matname, $($matfield : [$mattype; $matcount]),+);
		uniform_buffers!(@matimpl, $matname, $($matfield : [$mattype; $matcount]),+);
		uniform_buffers!(@matubofield, $matname, 0, 0, LAST_SCENE_BINDING + 1, $($matfield : [$mattype; $matcount]),+);
		uniform_buffers!(@matubotrait, $matname, $($matfield : [$mattype; $matcount]),+);
	}};

	(
		@scnstruct,
		$structname:ident,
		$($field:ident : $type:path),+ $(,)?
	) => {
		pub struct $structname {
			__buffer: $crate::gl::types::GLuint,
			__cpuside: std::ptr::NonNull<u8>,
			__size: usize,
			$($field: std::ptr::NonNull<$type>),+
		}
	};

	(@scnimpl, $structname:ident, $($field:ident : $type:path),+ $(,)?) => {
		impl $structname {
			uniform_buffers!(
				@scnnew,
				$structname,
				$($field : $type),+
			);
			uniform_buffers!(@scngetmutof, $structname);
		}
	};

	(@scnnew, $structname:ident, $($field:ident : $type:path),+ $(,)?) => {
		pub fn new($($field: $type),+) -> $structname {
			let gl_ubo_alignment = unsafe {
				use $crate::gl::UNIFORM_BUFFER_OFFSET_ALIGNMENT;
				let mut data = 0;
				$crate::gl::GetIntegerv(
					UNIFORM_BUFFER_OFFSET_ALIGNMENT,
					&mut data,
				);
				debug_assert!(
					data != 0,
					"error getting uniform buffer alignment",
				);
				data as usize
    			};
			dbg!(gl_ubo_alignment);
			let __size = uniform_buffers!(@totalsize,
				gl_ubo_alignment,
				$($type),+
			);
			dbg!(__size);
			let __buffer = uniform_buffers!(
				@scnbuffer,
				__size,
				gl_ubo_alignment,
				$($field : $type),+
			);
			let __cpuside = unsafe {
				use std::alloc::Layout;
				let layout = Layout::from_size_align_unchecked(
					__size,
					// Really no idea.
					std::mem::align_of::<[u8; 2]>(),
				);
				std::alloc::alloc(layout)
			};

			let mut ubo = uniform_buffers!(
				@scndefine,
				$structname,
				__buffer,
				__cpuside,
				__size,
				gl_ubo_alignment,
				0,
				$(@ $field : $type,)+
			);

			dbg!(ubo.__size);
			unsafe {
    				$(dbg!((ubo.$field.as_ptr() as *const u8).offset_from(ubo.__cpuside.as_ptr()));)+
			}
			$(dbg!((ubo.$field.as_ptr()).align_offset(core::mem::align_of::<$type>()));)+
			$(*ubo.get_mut_of::<$type>() = $field;)+

			ubo
		}
	};

	// Because macro calls are not allowed during struct parsing,
	// we build the struct definition recursively.
	// The tt holds both the remaining 'field: type' pairs at its
	// start, and the 'field: NonNull<type>' pairs are built from the
	// start of the tt back to its end.
	// When no 'field: type' remain, build the complete struct definition.
	// The '@' marker is used to differentiate 'field: type'
	// from 'field: NonNull<type>'
	(
		@scndefine,
		$structname:ident,
		$buffer:ident,
		$cpuside:ident,
		$size:ident,
		$align:ident,
		$offset:expr,
		@ $field:ident : $type:path,
		$($tokens:tt)*
	) => {
		uniform_buffers!(
			@scndefine,
			$structname,
			$buffer,
			$cpuside,
			$size,
			$align,
			$offset + uniform_buffers!(
    				@alignedsize,
				$align,
				$type
			),
			$($tokens)*
			$field : unsafe { std::ptr::NonNull::new_unchecked(
				$cpuside.add($offset) as *mut $type
			) },
		);
	};

	(
		@scndefine,
		$structname:ident,
		$buffer:ident,
		$cpuside:ident,
		$size:ident,
		$align:ident,
		$offset:expr,
		$($tokens:tt)+
	) => {
		$structname {
				__buffer: $buffer,
				__cpuside : unsafe {
					std::ptr::NonNull::new_unchecked(
						$cpuside
					)
				},
				__size: $size,
				$($tokens)+
		}
	};

	(
		@scnbuffer,
		$size:ident,
		$align:ident,
		$($field:ident : $type:path),+ $(,)?
	) => {
		unsafe {
			let mut __buffer = 0;
			$crate::gl::CreateBuffers(1, &mut __buffer as *mut _);
			// TODO: why not an Error?
			debug_assert!(
				__buffer != 0,
				"error creating ubo",
			);
			$crate::gl::BindBuffer(
				$crate::gl::UNIFORM_BUFFER,
				__buffer,
			);
			$crate::gl::BufferData(
				$crate::gl::UNIFORM_BUFFER,
				$size as gl::types::GLsizeiptr,
				std::ptr::null()
					as *const $crate::gl::types::GLvoid,
				$crate::gl::DYNAMIC_DRAW,
			);
			let better_be_36 = unsafe {
    				let mut v = 0;
    				$crate::gl::GetIntegerv($crate::gl::MAX_UNIFORM_BUFFER_BINDINGS, &mut v);
    				v
			};
			dbg!(better_be_36);
			uniform_buffers!(
				@scnbindrange,
				__buffer,
				$align,
				0,
				0,
				$($type),+
			);
			__buffer
		}
	};

	(
		@scnuboget,
		$structname:ident,
		$binding:expr,
		$field:ident : $type:ty,
		$($tail:tt)+
	) => {
		unsafe impl $crate::ubo::UboGet<$structname> for $type {
			const BINDING: $crate::gl::types::GLuint = $binding;
			const NAME: *const $crate::gl::types::GLchar
				= $crate::c_lit!($field);

			fn get_mut_of<'a>(
				ubo: &'a mut $structname
			) -> &'a mut $type {
				// Safety: lifetime contrained by the Ubo,
				// which does actually hold the buffer data.
				unsafe { ubo.$field.as_mut() }
			}
		}

		uniform_buffers!(
			@scnuboget,
			$structname,
			$binding + 1,
			$($tail)+
		);
	};

	(
		@scnuboget,
		$structname:ident,
		$binding:expr,
		$field:ident : $type:ty $(,)?
	) => {
		unsafe impl $crate::ubo::UboGet<$structname> for $type {
			const BINDING: $crate::gl::types::GLuint = $binding;
			const NAME: *const $crate::gl::types::GLchar
				= $crate::c_lit!($field);

			fn get_mut_of<'a>(
				ubo: &'a mut $structname
			) -> &'a mut $type {
				// Safety: lifetime contrained by the Ubo,
				// which does actually hold the buffer data.
				unsafe { ubo.$field.as_mut() }
			}
		}

		const LAST_SCENE_BINDING: $crate::gl::types::GLuint =
			$binding;
	};

	(@scngetmutof, $structname:ident) => {
		pub fn get_mut_of<T>(&mut self) -> &mut T
		where T: $crate::ubo::UboGet<$structname> {
			<T as $crate::ubo::UboGet<$structname>>::get_mut_of(
				self
			)
		}
	};

	(@scnubo, $structname:ident) => {
		unsafe impl $crate::ubo::Ubo for $structname {
			unsafe fn buffer_data(&self) {
				// For now, assume the buffer is already
				// bound.
				$crate::gl::BufferSubData(
					$crate::gl::UNIFORM_BUFFER,
					0,
					self.__size as gl::types::GLsizeiptr,
					self.__cpuside.as_ptr()
						as *const gl::types::GLvoid,
				)
			}

			unsafe fn gl_name(&self) -> GLuint {
    				self.__buffer
			}
		}
	};

	(@scndrop, $structname:ident) => {
		impl Drop for $structname {
			fn drop(&mut self) {
				let align = std::mem::align_of::<[u8; 2]>();
				use std::alloc::Layout;
				let layout = unsafe {
					Layout::from_size_align_unchecked(
						self.__size,
						align,
					)
				};
				unsafe {
					std::alloc::dealloc(
						self.__cpuside.as_ptr(),
						layout,
					);
					$crate::gl::DeleteBuffers(
						1,
						&self.__buffer
					);
				}
			}
		}
	};

	(@totalsize, $align:ident, $type:path, $($tail:tt)+) => {
		(uniform_buffers!(@alignedsize, $align, $type)
			+ uniform_buffers!(@totalsize, $align, $($tail)+));
	};

	(@totalsize, $align:ident, $type:path $(,)?) => {
		uniform_buffers!(@alignedsize, $align, $type)
	};

	(
		@scnbindrange,
		$buffer:ident,
		$align:ident,
		$idx:expr,
		$offset:expr,
		$type:path,
		$($tail:tt)+
	) => {
    		let size = core::mem::size_of::<$type>();
		let aligned_size = uniform_buffers!(
    			@alignedsize,
			$align,
			$type
		);
		$crate::gl::BindBufferRange(
			$crate::gl::UNIFORM_BUFFER,
			$idx,
			$buffer,
			$offset,
			size as $crate::gl::types::GLsizeiptr,
		);
		eprintln!("bound {} offset {} size {}", $idx, $offset, size); $crate::glerr!();
		uniform_buffers!(
			@scnbindrange,
			$buffer,
			$align,
			$idx + 1,
			$offset + aligned_size as $crate::gl::types::GLsizeiptr,
			$($tail)+
		)
	};

	(
		@scnbindrange,
		$buffer:ident,
		$align:ident,
		$idx:expr,
		$offset:expr,
		$type:path
	) => {
    		let size = core::mem::size_of::<$type>();
		let aligned_size = uniform_buffers!(
    			@alignedsize,
			$align,
			$type
		);
		$crate::gl::BindBufferRange(
			$crate::gl::UNIFORM_BUFFER,
			$idx,
			$buffer,
			$offset,
			size as $crate::gl::types::GLsizeiptr,
		);
		eprintln!("bound {} offset {} size {}", $idx, $offset, size); $crate::glerr!();
	};

	(@alignedsize, $align:ident, $type:path) => {
		((std::mem::size_of::<$type>() + ($align - 1)) / $align)
			* $align
	};

	(@matstruct, $structname:ident, $($field:ident : [$type:path; $count:literal]),+) => {
		pub struct $structname {
			buffer__: $crate::gl::types::GLuint,
			// (Offsets into the GL buffer, store).
			$($field: ($crate::gl::types::GLintptr, [$type; $count])),+
		}
	};

	(@matimpl, $structname:ident, $($field:ident : [$type:path; $matcount:literal]),+ $(,)?) => {
    		impl $structname {
			uniform_buffers!(
				@matnew,
				$structname,
				$($field : [$type; $matcount]),+
			);
    		}

    		impl Drop for $structname {
	    		fn drop(&mut self) {
				unsafe {
					$crate::gl::DeleteBuffers(
						1,
						&self.buffer__
					);
				}
	    		}
    		}
	};

	(@matnew, $structname:ident, $($field:ident : [$type:path; $matcount:literal]),+ $(,)?) => {
    		pub fn new($($field : [$type; $matcount]),+) -> $structname {
			let gl_ubo_alignment = unsafe {
				use $crate::gl::UNIFORM_BUFFER_OFFSET_ALIGNMENT;
				let mut data = 0;
				$crate::gl::GetIntegerv(
					UNIFORM_BUFFER_OFFSET_ALIGNMENT,
					&mut data,
				);
				debug_assert!(
					data != 0,
					"error getting uniform buffer alignment",
				);
				data as usize
    			};
			dbg!(gl_ubo_alignment);
			let size = uniform_buffers!(@totalsize,
				gl_ubo_alignment,
				$($type),+
			);
			$( eprintln!("binding {} store mask {:b}", <$type as $crate::ubo::MaterialUboField<$structname>>::BINDING, <$type as $crate::ubo::MaterialUboField<$structname>>::STORE_MASK); )+
			let buffer__ = unsafe {
    				let mut buffer = 0;
    				$crate::gl::CreateBuffers(
        				1,
					&mut buffer as *mut _,
    				);
    				debug_assert!(buffer != 0, "error creating ubo");
    				$crate::gl::BindBuffer(
        				$crate::gl::UNIFORM_BUFFER,
        				buffer,
    				);
    				dbg!(size);
    				$crate::gl::BufferData(
        				$crate::gl::UNIFORM_BUFFER,
        				size as $crate::gl::types::GLsizeiptr,
        				core::ptr::null(),
        				$crate::gl::DYNAMIC_DRAW,
    				);
    				buffer
			};
    			// Reusing @scnbindrange, not a mistake (might want to rename then).
    			unsafe { uniform_buffers!(@scnbindrange, buffer__, gl_ubo_alignment, LAST_SCENE_BINDING + 1, 0, $($type),+); }


			let mut ubo = uniform_buffers!(@matdefine, $structname, buffer__, gl_ubo_alignment, 0, $(@ $field : [$type; $matcount],)+);
			// The GL buffer is still bound, calling
			// buffer_subdata is ok.
			unsafe {
				$( <$type as $crate::ubo::MaterialUboField<$structname>>::buffer_subdata(&$field[0], &ubo); )+
			}

			ubo
    		}
	};

	// Same deal as @scndefine.
	(
		@matdefine,
		$structname:ident,
		$buffer:ident,
		$align:ident,
		$offset:expr,
		@ $field:ident : [$type:path; $count:literal],
		$($tokens:tt)*
	) => {
		uniform_buffers!(
			@matdefine,
			$structname,
			$buffer,
			$align,
			$offset + uniform_buffers!(
    				@alignedsize,
				$align,
				$type
			),
			$($tokens)*
			$field : ($offset as $crate::gl::types::GLintptr, $field),
		);
	};

	(
		@matdefine,
		$structname:ident,
		$buffer:ident,
		$align:ident,
		$offset:expr,
		$($tokens:tt)+
	) => {
		$structname {
				buffer__: $buffer,
				$($tokens)+
		}
	};

	(@matubofield, $matname:ident, $fieldnumber:expr, $bitoffset:expr, $binding:expr, $matfield:ident : [$mattype:path; $matcount:literal], $($tail:tt)+) => {
		impl $crate::ubo::MaterialUboField<$matname> for $mattype {
			const FIELD_NUMBER: usize = $fieldnumber;
			const BIT_OFFSET: $crate::ubo::MaterialUboKeyBits = $bitoffset;
			const BIT_LEN: $crate::ubo::MaterialUboKeyBits = $crate::ubo::MaterialUboKeyBits::next_power_of_two($matcount + 1).trailing_zeros();
			const STORE_MASK: $crate::ubo::MaterialUboKeyBits = ((1 << Self::BIT_LEN) - 1) << Self::BIT_OFFSET;
			const BINDING: $crate::gl::types::GLuint = $binding;
			const NAME: *const $crate::gl::types::GLchar = $crate::c_lit!($matfield);
			/// Upload to a material UBO.
			///
			/// # Safety
			///
			/// Assumes the UBO is bound.
			unsafe fn buffer_subdata(&self, ubo: &$matname) {
				$crate::gl::BufferSubData(
					$crate::gl::UNIFORM_BUFFER,
					ubo.$matfield.0,
					core::mem::size_of::<$mattype>() as $crate::gl::types::GLsizeiptr,
					self as *const _ as *const $crate::gl::types::GLvoid,
				);
			}
		}

		uniform_buffers!(@matubofield, $matname, $fieldnumber + 1, $bitoffset + <$mattype as $crate::ubo::MaterialUboField<$matname>>::BIT_LEN, $binding + 1, $($tail)+);
	};

	(@matubofield, $matname:ident, $fieldnumber:expr, $bitoffset:expr, $binding:expr, $matfield:ident : [$mattype:path; $matcount:literal] $(,)?) => {
		impl $crate::ubo::MaterialUboField<$matname> for $mattype {
			const FIELD_NUMBER: usize = $fieldnumber;
			const BIT_OFFSET: $crate::ubo::MaterialUboKeyBits = $bitoffset;
			const BIT_LEN: $crate::ubo::MaterialUboKeyBits = $crate::ubo::MaterialUboKeyBits::next_power_of_two($matcount + 1).trailing_zeros();
			const STORE_MASK: $crate::ubo::MaterialUboKeyBits = ((1 << Self::BIT_LEN) - 1) << Self::BIT_OFFSET;
			const BINDING: $crate::gl::types::GLuint = $binding;
			const NAME: *const $crate::gl::types::GLchar = $crate::c_lit!($matfield);
			/// Upload to a material UBO.
			///
			/// # Safety
			///
			/// Assumes the UBO is bound.
			unsafe fn buffer_subdata(&self, ubo: &$matname) {
				$crate::gl::BufferSubData(
					$crate::gl::UNIFORM_BUFFER,
					ubo.$matfield.0,
					core::mem::size_of::<$mattype>() as $crate::gl::types::GLsizeiptr,
					self as *const _ as *const $crate::gl::types::GLvoid,
				);
			}
		}
	};

	(@matubotrait, $structname:ident, $($field:ident : [$type:path; $count:literal]),+) => {
		impl $crate::ubo::MaterialUbo for $structname {
			type StoreIdx =  [Option<usize>; 0 $( + { core::marker::PhantomData::<$type>; 1 })+];

			fn gl_name(&self) -> GLuint {
				self.buffer__
			}

			// In debug, guarented to generate a valid key for the MaterialUbo type.
			fn key_for(&self, indices: Self::StoreIdx) -> $crate::ubo::MaterialUboKey {
				use $crate::ubo::MaterialUboField;
				use $crate::ubo::MaterialUboKeyBits;
				$( if let Some(idx) = indices[<$type as MaterialUboField<$structname>>::FIELD_NUMBER] {
    					debug_assert!(idx < $count, "material index out of store");
				} )+
				let mut bits = 0;
				$( let mat_bits = match indices[<$type as MaterialUboField<$structname>>::FIELD_NUMBER] {
    					Some(idx) => (idx as MaterialUboKeyBits & ((1 << <$type as MaterialUboField<$structname>>::BIT_LEN) - 1)) << <$type as MaterialUboField<$structname>>::BIT_OFFSET,
    					None => <$type as MaterialUboField<$structname>>::STORE_MASK,
				};
				bits |= mat_bits; )+
				$crate::ubo::MaterialUboKey(bits)
			}

			// Ignores key that translate to invalid indices.
			// Assumes the UBO is bound.
			// TODO: bind only the difference to another key (with the idea of sorting keys).
			unsafe fn bind_key(&self, key: $crate::ubo::MaterialUboKey) {
				use $crate::ubo::MaterialUboField;
				$(
    				let idx = ((key.0 & <$type as MaterialUboField<$structname>>::STORE_MASK) >> <$type as MaterialUboField<$structname>>::BIT_OFFSET) as usize;
    				if idx < self.$field.1.len() {
    					let to_bind = &self.$field.1[idx];
					<$type as MaterialUboField<$structname>>::buffer_subdata(to_bind, self);
				})+
			}
		}
	};
}
