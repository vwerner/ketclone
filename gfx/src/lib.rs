//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

#![allow(clippy::zero_ptr)]

// Re-exports.
pub use gl;
pub use glam;

use assets_fbs::model::generated::DrawCommand as FbsDrawCommand;
use assets_fbs::model::generated::{Model, Primitive};
use assets_fbs::model::header::Header;
use assets_fbs::texture_atlas::generated::Rectangle as FbsRectangle;
use assets_fbs::texture_atlas::generated::{
	root_as_texture_atlas, PixelFormat,
};
use gl::types::*;
use glam::swizzles::*;
use glam::{const_uvec2, const_vec2, Mat3, Mat4, UVec2, Vec2, Vec4};
use std::fs::File;
use std::io::{self, Read, Write};
use std::marker::PhantomData;
use std::mem;
use std::path::Path;
use std::ptr::{self, NonNull};
use std::slice;

pub mod ubo;
#[doc(inline)]
pub use ubo::{MaterialUbo, MaterialUboField, MaterialUboKey, Ubo, UboGet};
pub mod scene;

/// Image unit used for color textures.
const COLOR_TEXTURE_UNIT: GLint = 0;

/// Converts tokens into a C-string literal (C-char pointer).
///
/// The null terminator is appended by the macro. Do not add quotes, and do not
/// try to add a null char.
///
/// This macro is only exported for the ['uniform_buffer'] macro and
/// shouldn't be of much use in user code.
#[macro_export]
#[doc(hidden)]
macro_rules! c_lit {
	($($tok:tt)*) => {
    		// Rely on as_bytes being const fn.
		concat!(stringify!($($tok)*), "\0")
			.as_bytes()
			.as_ptr() as *const GLchar
	};
}

/// Name of the model-to-camera mat4 uniform in all programs.
const MODEL_TO_CAMERA_NAME: *const GLchar = c_lit!(model_to_camera);
/// Name of the normal model-to-camera mat3 uniform in all programs.
const NORMAL_MODEL_TO_CAMERA_NAME: *const GLchar =
	c_lit!(normal_model_to_camera);

/// Compute a normal model-to-camera matrix from the vertices' one.
fn compute_normal_model_to_camera(model_to_camera: &glam::Mat4) -> glam::Mat3 {
	glam::Mat3::from_cols(
		model_to_camera.x_axis.xyz(),
		model_to_camera.y_axis.xyz(),
		model_to_camera.z_axis.xyz(),
	)
	.inverse()
	.transpose()
}

/// Probe all OpenGL errors that were raised and write them to a buffer.
#[cfg(build = "debug")]
pub fn write_gl_errors<W: Write>(mut buf: W) -> io::Result<()> {
	loop {
		unsafe {
			let glerr = gl::GetError();
			if glerr == gl::NO_ERROR {
				return Ok(());
			}
			let res = buf.write_all(match glerr {
				gl::INVALID_ENUM => b" GL_INVALID_ENUM",
				gl::INVALID_VALUE => b" GL_INVALID_VALUE",
				gl::INVALID_OPERATION => {
					b" GL_INVALID_OPERATION"
				}
				gl::INVALID_FRAMEBUFFER_OPERATION => {
					b" GL_INVALID_FRAMEBUFFER_OPERATION"
				}
				gl::OUT_OF_MEMORY => b" GL_OUT_OF_MEMORY",
				gl::STACK_UNDERFLOW => b" GL_STACK_UNDERFLOW",
				gl::STACK_OVERFLOW => b" GL_STACK_OVERFLOW",
				gl::CONTEXT_LOST => b" GL_CONTEXT_LOST",
				_ => unreachable!(),
			});
			if res.is_err() {
				return res;
			};
		}
	}
}

/// Print raised errors after a given call to stderr.
///
/// Errors raised prior to the call will also be printed.
// No-op on release builds.
#[macro_export]
macro_rules! glerr {
	($call:expr) => {
		$crate::glerr!(@print, $call)
	};
	() => {
		$crate::glerr!(@print, "")
	};
	(@print, $funcname:expr) => { #[cfg(build = "debug")] {
		eprint!(
			"glerr {}:{}:{} {}",
			file!(),
			line!(),
			column!(),
			$funcname,
		);
		$crate::write_gl_errors(std::io::stderr())
			.expect("failed to write the GL errors to stderr.");
		eprintln!();
	}};
}

/// Errors of this crate.
#[derive(Debug)]
pub enum Error {
	ShaderCreation,
	ShaderCompilation(String),
	ProgramCreation,
	ProgramLinkage(String),
	ModelStoreFull,
	StateStoreProgramFull,
}

type Result<T> = std::result::Result<T, Error>;

/// A compiled OpenGL fragment shader.
#[derive(Debug)]
pub(crate) struct VertexShader(GLuint);

/// Arguments to a glDrawElementBaseVertex call.
///
/// Models are meant to be read from an external file which supply their
/// own format of draw commands, which may differ from this format here for
/// a variety of reason. More importantly, the arguments must be kept around
/// after reading a model file; in comparison, vertex data can be discarded
/// once they are sent to GPU. Finally, only the game's code is 100% sure
/// of the types expected for the argument. This is the reasoning behind
/// defining a type proper to the ModelStore.
#[derive(Clone, Copy, Debug)]
struct DrawCommand {
	primitive: GLenum,
	count: GLsizei,
	// type: always GLushort,
	indices: *const GLvoid,
	basevertex: GLint,
}

impl DrawCommand {
	/// A draw command that will have no effect.
	///
	/// Meant to represent an absence of a draw command in a
	/// ModelStore. Could still trigger a GL error, I'm not sure.
	const NO_OP: DrawCommand = DrawCommand {
		primitive: gl::POINT,
		count: 0,
		indices: 0 as *const GLvoid,
		basevertex: 0,
	};

	fn is_noop(&self) -> bool {
		self.primitive == DrawCommand::NO_OP.primitive
			&& self.count == DrawCommand::NO_OP.count
			&& self.indices == DrawCommand::NO_OP.indices
			&& self.basevertex == DrawCommand::NO_OP.basevertex
	}

	/// Call glDrawElementBaseVertex.
	///
	/// Make no assumption about the current drawing state.
	unsafe fn draw(&self) {
		gl::DrawElementsBaseVertex(
			self.primitive,
			self.count,
			gl::UNSIGNED_SHORT,
			self.indices,
			self.basevertex,
		);
	}
}

impl From<FbsDrawCommand> for DrawCommand {
	// *NOTE*: The generated DrawCommand implements Copy.
	fn from(cmd: FbsDrawCommand) -> DrawCommand {
		let primitive = match cmd.primitive() {
			Primitive::Triangle => gl::TRIANGLES,
			_ => unimplemented!("unimplemented primitive in the flatbuffer model"),
		};

		DrawCommand {
			primitive,
			count: cmd.count(),
			indices: cmd.indices() as *const GLvoid,
			basevertex: cmd.basevertex(),
		}
	}
}

/// Describes a set of models stored in the same set of buffers.
///
/// Holds the necessary GL state (buffers and vertex array) and holds the
/// draw commands arguments for each model in the buffer. These buffers
/// are allocated at initialization, and are destroyed when dropped.
///
/// # Notes
///
/// Assume the VBO, IBO and VAO of the model store are the only object of
/// these sort that will ever be bound during the program. Assume there's
/// ever a single ModelStore during the program's life.
pub struct ModelStore {
	// [VBO, IBO].
	buffers: [GLuint; 2],
	vao: GLuint,
	cmds: [DrawCommand; 16],
	// Start indices in cmds for each model.
	// TODO: check std::pin. It allow self-referencial structs and should
	// allow me to replace model_indices by an array of commands slices:
	// like cmds_for, but directly.
	model_indices: [usize; 16 + 1],
}

impl ModelStore {
	// Per vertex, 3 positions floats, 3 normals floats,
	// 2 uv floats.
	const SIZE_PER_POS: GLsizeiptr =
		3 * mem::size_of::<GLfloat>() as GLsizeiptr;
	const SIZE_PER_NORMAL: GLsizeiptr =
		3 * mem::size_of::<GLfloat>() as GLsizeiptr;
	const SIZE_PER_UV: GLsizeiptr =
		2 * mem::size_of::<GLfloat>() as GLsizeiptr;
	const SIZE_PER_VERTEX: GLsizeiptr =
		Self::SIZE_PER_POS + Self::SIZE_PER_NORMAL + Self::SIZE_PER_UV;

	/// Create a ModelStore with models loaded from files.
	pub fn with_model_files<P>(paths: &[P]) -> ModelStore
	where
		P: AsRef<Path>,
	{
		let mut store = ModelStore::empty();
		store.store_model_files(paths);
		store
	}

	/// Create an empty ModelStore.
	///
	/// The store's buffers are created but unallocated.
	fn empty() -> ModelStore {
		let mut store = ModelStore {
			buffers: [0, 0],
			vao: 0,
			cmds: [DrawCommand::NO_OP; 16],
			model_indices: [16; 17],
		};
		unsafe {
			gl::CreateBuffers(2, store.buffers.as_mut_ptr());
			gl::GenVertexArrays(1, &mut store.vao as *mut _);
		}
		// TODO: check for errors when creating the buffer/vao.
		store
	}

	/// Get the commands for the given model index.
	fn cmds_for(&self, model_idx: usize) -> &[DrawCommand] {
		debug_assert!(model_idx < 16);
		let start = self.model_indices[model_idx];
		let end = self.model_indices[model_idx + 1];
		&self.cmds[start..end]
	}

	/// Compute the total number of vertices and indices of a set of model files.
	///
	/// This only read the header of each file.
	fn peek_model_headers<P>(paths: &[P]) -> (GLushort, GLsizeiptr)
	where
		P: AsRef<Path>,
	{
		let mut buf: [u8; Header::BYTE_SIZE] = [0; Header::BYTE_SIZE];
		let mut total_vertices_count = 0;
		let mut total_indices_count = 0;
		for p in paths {
			assets_fbs::model::read::read_header(p, &mut buf)
				.expect("failed to read model file header");
			let header =
				Header::from_bytes_exact(Clone::clone(&buf));
			total_vertices_count += header.vertices_count;
			total_indices_count += header.indices_count;
		}

		dbg!(total_vertices_count);

		(total_vertices_count, total_indices_count as GLsizeiptr)
	}

	/// Allocate the store's buffers and setup the VAO.
	///
	/// Returns the offsets for each attributes.
	///
	/// *NOTE*: the VBO, IBO and VAO are bound after calling this function.
	// TODO: See if I can't reduce the repetition. Maybe with a structure
	// of constant arrays with attribute index, size...
	unsafe fn allocate_store(
		&mut self,
		vertices_count: GLushort,
		indices_count: GLsizeiptr,
	) -> [GLsizeiptr; 3] {
		let vbo_size =
			Self::SIZE_PER_VERTEX * vertices_count as GLsizeiptr;
		let ibo_size = indices_count as GLsizeiptr
			* mem::size_of::<GLushort>() as GLsizeiptr;
		// Somehow, just calling BindVertexArray reset the bound
		// element buffer to 0. I mean I don't know, why? Why isn't
		// there anything about this in the documentation?
		// What the fuck?
		gl::BindVertexArray(self.vao);
		gl::BindBuffer(gl::ARRAY_BUFFER, self.buffers[0]);
		gl::BufferData(
			gl::ARRAY_BUFFER,
			vbo_size,
			ptr::null(),
			gl::STATIC_DRAW,
		);
		gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.buffers[1]);
		gl::BufferData(
			gl::ELEMENT_ARRAY_BUFFER,
			ibo_size,
			ptr::null(),
			gl::STATIC_DRAW,
		);

		let pos_offset = 0;
		let normal_offset =
			Self::SIZE_PER_POS * vertices_count as GLsizeiptr;
		let uv_offset = (Self::SIZE_PER_POS + Self::SIZE_PER_NORMAL)
			* vertices_count as GLsizeiptr;
		// Position attribute (0).
		gl::EnableVertexAttribArray(0);
		gl::VertexAttribPointer(
			0,
			3,
			gl::FLOAT,
			gl::FALSE,
			0,
			pos_offset as *const GLvoid,
		);
		// Normals attribute (1).
		gl::EnableVertexAttribArray(1);
		gl::VertexAttribPointer(
			1,
			3,
			gl::FLOAT,
			gl::FALSE,
			0,
			normal_offset as *const GLvoid,
		);
		// UVs attributes (2).
		gl::EnableVertexAttribArray(2);
		gl::VertexAttribPointer(
			2,
			2,
			gl::FLOAT,
			gl::FALSE,
			0,
			uv_offset as *const GLvoid,
		);

		[pos_offset, normal_offset, uv_offset]
	}

	/// Push a model into the store.
	///
	/// Push the vertex data, index data, and draw commands at the
	/// given offsets, and return the new offsets for the next push.
	/// ```text
	/// attr_offsets[0]┄┄┄┄┐         attr_offsets[1]┄┄┄┄┐
	///                    ┆                            ┆
	///                    ↓                            ↓
	/// ┏VBO━━━━━━━━━━━━━━━┯━━━━━━━━━┯━━━━━━━━━━━━━━━━━━┯━━━━━━┅
	/// ┃ previous v. data │  [...]  │ previous n. data │ [...]
	/// ┗━━━━━━━━━━━━━━━━━━┷━━━━━━━━━┷━━━━━━━━━━━━━━━━━━┷━━━━━━┅
	/// ```
	// TODO: Same here, could probably reduce the repetition.
	unsafe fn push_model(
		&mut self,
		model: &Model,
		attr_offsets: [GLsizeiptr; 3],
		index_offset: GLsizeiptr,
		basevertex: GLint,
		cmd_offset: usize,
	) -> ([GLsizeiptr; 3], GLsizeiptr, GLint, usize) {
		let pos_data = model.pos().unwrap();
		let normals_data = model.normals().unwrap();
		let uvs_data = model.uvs().unwrap();
		debug_assert!(
			pos_data.len() == normals_data.len(),
			"attribute count mismatch"
		);
		debug_assert!(
			pos_data.len() == uvs_data.len(),
			"attribute count mismatch"
		);
		let vertex_count = pos_data.len();
		dbg!(vertex_count);
		let pos_size = Self::SIZE_PER_POS * vertex_count as GLsizeiptr;
		let normals_size =
			Self::SIZE_PER_NORMAL * vertex_count as GLsizeiptr;
		let uvs_size = Self::SIZE_PER_UV * vertex_count as GLsizeiptr;

		// gl::BindBuffer(gl::ARRAY_BUFFER, self.buffers[0]);
		gl::BufferSubData(
			gl::ARRAY_BUFFER,
			attr_offsets[0],
			pos_size,
			pos_data.as_ptr() as *const GLvoid,
		);
		gl::BufferSubData(
			gl::ARRAY_BUFFER,
			attr_offsets[1],
			normals_size,
			normals_data.as_ptr() as *const GLvoid,
		);
		gl::BufferSubData(
			gl::ARRAY_BUFFER,
			attr_offsets[2],
			uvs_size,
			uvs_data.as_ptr() as *const GLvoid,
		);

		let indices_data = model.indices().unwrap().safe_slice();
		let indices_size = (indices_data.len()
			* mem::size_of::<GLushort>()) as GLsizeiptr;
		// gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.buffers[1]);
		gl::BufferSubData(
			gl::ELEMENT_ARRAY_BUFFER,
			index_offset,
			indices_size,
			indices_data.as_ptr() as *const GLvoid,
		);

		let model_idx = self
			.cmds
			.iter()
			.enumerate()
			.find(|(_i, cmd)| cmd.is_noop())
			.expect("couldn't push model: no free slot")
			.0;

		// *TODO*: use a range slice and a zipped iterator instead.
		let mut cmds_count = 0;
		for cmd in model.cmds().unwrap() {
			self.cmds[cmd_offset + cmds_count] = From::from(*cmd);
			// Am I sure about that:
			self.cmds[cmd_offset + cmds_count].indices =
				index_offset as *const GLvoid;
			self.cmds[cmd_offset + cmds_count].basevertex =
				basevertex;
			// *NOTE*: not checking for out-of-range.
			cmds_count += 1;
		}
		self.model_indices[model_idx] = cmd_offset;
		// Set the next model command offset to one-past-the-end
		// of this model's.
		self.model_indices[model_idx + 1] = cmd_offset + 1;

		let new_attr_offsets = [
			attr_offsets[0] + pos_size,
			attr_offsets[1] + normals_size,
			attr_offsets[2] + uvs_size,
		];
		let new_indices_offset = index_offset + indices_size;
		let new_basevertex = basevertex + vertex_count as GLint;
		let new_cmds_offset = cmd_offset + cmds_count;
		(
			new_attr_offsets,
			new_indices_offset,
			new_basevertex,
			new_cmds_offset,
		)
	}

	/// Read the given model files into the store.
	///
	/// This wipes and reallocate the buffers.
	///
	/// *TODO*: Later, I might want to reuse those buffers and skip
	/// the reallocation.
	pub fn store_model_files<P>(&mut self, paths: &[P])
	where
		P: AsRef<Path>,
	{
		let (vertices_count, indices_count) =
			ModelStore::peek_model_headers(paths);
		let attr_offsets = unsafe {
			self.allocate_store(vertices_count, indices_count)
		};

		// Fill up the buffer data and draw commands.
		let mut buf: Vec<u8> = Vec::with_capacity(4096);
		let mut offsets = (attr_offsets, 0, 0, 0);
		for p in paths {
			buf.clear();
			assets_fbs::model::read::read_all_to_end(p, &mut buf)
				.expect("failed to read model file");
			let model =
				assets_fbs::model::generated::root_as_model(
					&buf[Header::BYTE_SIZE..],
				)
				.expect("invalid model flatbuffer");
			offsets = unsafe {
				self.push_model(
					&model, offsets.0, offsets.1,
					offsets.2, offsets.3,
				)
			};
		}
	}
}

impl Drop for ModelStore {
	/// Delete the VBO, IBO and VAO.
	fn drop(&mut self) {
		unsafe {
			gl::DeleteBuffers(2, self.buffers.as_ptr());
			gl::DeleteVertexArrays(1, &self.vao as *const _);
		}
	}
}

/// Compile a shader from a slice of bytes.
// Assumes `kind` is either VERTEX_SHADER or FRAGMENT_SHADER.
unsafe fn compile_shader(src: &[GLchar], kind: GLenum) -> Result<GLuint> {
	let shader = gl::CreateShader(kind);
	if shader == 0 {
		return Err(Error::ShaderCreation);
	}
	gl::ShaderSource(shader, 1, &src.as_ptr(), &(src.len() as GLint));
	gl::CompileShader(shader);
	let mut status = gl::FALSE as GLint;
	gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut status);
	if status != gl::TRUE as GLint {
		return Err(Error::ShaderCompilation(shader_info_log(shader)));
	}
	Result::Ok(shader)
}

/// Compile a vertex shader from a slice of bytes.
unsafe fn compile_vertex_shader(src: &[u8]) -> Result<GLuint> {
	compile_shader(
		&*(src as *const [u8] as *const [GLchar]),
		gl::VERTEX_SHADER,
	)
}

/// Compile a fragment shader from a slice of bytes.
unsafe fn compile_fragment_shader(src: &[u8]) -> Result<GLuint> {
	compile_shader(
		&*(src as *const [u8] as *const [GLchar]),
		gl::FRAGMENT_SHADER,
	)
}

/// Link a program from a vertex and a fragment shader.
// Assumes `vs`, `fs` are ids for valid vertex/fragment shader objects.
unsafe fn link_program(vs: GLuint, fs: GLuint) -> Result<GLuint> {
	let program = gl::CreateProgram();
	if program == 0 {
		return Err(Error::ProgramCreation);
	}
	gl::AttachShader(program, vs);
	gl::AttachShader(program, fs);
	gl::LinkProgram(program);

	let mut status = gl::FALSE as GLint;
	gl::GetProgramiv(program, gl::LINK_STATUS, &mut status);
	if status != gl::TRUE as GLint {
		return Err(Error::ProgramLinkage(program_info_log(program)));
	}
	Result::Ok(program)
}

/// Retrieve a shader's info log (e.g. compilation errors).
// Assumes `shader` identifies a valid shader object.
unsafe fn shader_info_log(shader: GLuint) -> String {
	let mut len = 0;
	gl::GetShaderiv(shader, gl::INFO_LOG_LENGTH, &mut len);
	let len = len - 1; // Discard null character.
	let mut buf = Vec::with_capacity(len as usize);
	buf.set_len(len as usize);
	gl::GetShaderInfoLog(
		shader,
		len,
		ptr::null_mut(),
		buf.as_mut_ptr() as *mut GLchar,
	);
	String::from_utf8_unchecked(buf)
}

/// Retrieve a program's info log (e.g. linkage errors).
// Assumes `program` identifies a valid program object.
unsafe fn program_info_log(program: GLuint) -> String {
	let mut len = 0;
	gl::GetProgramiv(program, gl::INFO_LOG_LENGTH, &mut len);
	let len = len - 1; // Discard null character.
	let mut buf = Vec::with_capacity(len as usize);
	buf.set_len(len as usize);
	gl::GetProgramInfoLog(
		program,
		len,
		ptr::null_mut(),
		buf.as_mut_ptr() as *mut GLchar,
	);
	String::from_utf8_unchecked(buf)
}

/// Load a 2D texture from a flatbuffer texture atlas.
///
/// Also returns the list of rectangles in the atlas, if any.
fn load_fbs_texture(buf: &[u8]) -> TextureAtlas {
	let atlas =
		root_as_texture_atlas(buf).expect("invalid texture flatbuffer");
	let w = atlas.width();
	let h = atlas.height();
	let (internalformat, format) = match atlas.format() {
		PixelFormat::RGB => (gl::RGB8, gl::RGB),
		PixelFormat::RGBA => (gl::RGBA8, gl::RGBA),
		_ => unreachable!("bad pixel format"),
	};
	let image_data = atlas
		.image_data()
		.expect("texture atlas flatbuffer has no image data");

	let mut name = 0;
	unsafe {
		gl::GenTextures(1, &mut name);
		if name == 0 {
			panic!("unknown error during gl texture generation")
		}
		gl::BindTexture(gl::TEXTURE_2D, name);
		// No mipmap, same data format and internal format.
		gl::TexImage2D(
			gl::TEXTURE_2D,
			0,
			internalformat as GLint, // cast is ok trust me.
			w as GLsizei,
			h as GLsizei,
			0,
			format,
			gl::UNSIGNED_BYTE,
			image_data.as_ptr() as *const GLvoid,
		);
		gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_BASE_LEVEL, 0);
		gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAX_LEVEL, 0);
	}

	let mut rects_arr = [AtlasRectangle::NONE; TextureAtlas::RECTANGLE_MAX];
	if let Some(vec) = atlas.rects() {
		for (dst, src) in rects_arr.iter_mut().zip(vec.iter()) {
			*dst = From::from(*src);
		}
	}

	TextureAtlas {
		name,
		width: w,
		height: h,
		rects: rects_arr,
	}
}

/// An OpenGL program with its uniforms locations kept around.
///
/// Those uniforms are an optional mat4 (model-to-camera matrix) and an
/// optional mat3 (normal model-to-camera matrix). It also has an optional
/// color texture sampler: it is bound at construction, and its location
/// does not need to be remembered.
#[derive(Clone, Debug)]
struct Program {
	program: GLuint,
	model_to_camera_loc: GLint,
	normal_model_to_camera_loc: GLint,
}

impl Program {
	/// An absent program.
	const NO_OP: Program = Program {
		program: 0,
		model_to_camera_loc: 0,
		normal_model_to_camera_loc: 0,
	};

	/// Update the model-to-camera matrices using the given transform
	/// and camera.
	///
	/// Assume the program has been bound with `glUseProgram`. Both matrices
	/// are computed in this function, if the program needs them.
	unsafe fn update_model_to_camera_uniforms(
		&self,
		transform: &ModelWorldTransform,
		camera_look_at: &glam::Mat4,
	) {
		if self.model_to_camera_loc == -1 {
			// Program has no model-to-camera uniform, return.
			return;
		}

		let model_to_camera = *camera_look_at * transform.matrix();
		gl::UniformMatrix4fv(
			self.model_to_camera_loc,
			1,
			gl::FALSE,
			model_to_camera.as_ref() as *const GLfloat,
		);

		if self.normal_model_to_camera_loc == -1 {
			// Program has no normal matrix, return.
			return;
		}

		let normal_model_to_camera =
			compute_normal_model_to_camera(&model_to_camera);
		gl::UniformMatrix3fv(
			self.normal_model_to_camera_loc,
			1,
			gl::FALSE,
			normal_model_to_camera.as_ref() as *const GLfloat,
		);
	}
}

/// Enumeration of possible winding order.
///
/// Part of the render state.
pub enum WindingOrder {
	CCW,
	CW,
}

/// Holds the necessary GL state needed to draw a model.
///
/// A list of programs, texture, etc. The draw state is defined as the
/// combinaison of each item.
///
/// # Note
///
/// It is assumed only one such store exists during the entire program.
pub struct RenderStateStore {
	programs: [Program; 8],
	// Not part of the rendering state by themselves for necessary for programs.
	vertex_shaders: [GLuint; 8],
	fragment_shaders: [GLuint; 8],
	color_textures: [GLuint; 8],
	sampler_2d: GLuint,
}

impl RenderStateStore {
	/// Compiles the shaders and and store them.
	///
	/// For use in the constructor.
	unsafe fn setup_shaders(&mut self, vs_src: &[&[u8]], fs_src: &[&[u8]]) {
		let it = self.vertex_shaders.iter_mut().zip(vs_src.iter());
		for (slot, src) in it {
			*slot = compile_vertex_shader(src).unwrap();
		}
		let it = self.fragment_shaders.iter_mut().zip(fs_src.iter());
		for (slot, src) in it {
			*slot = compile_fragment_shader(src).unwrap();
		}
	}

	/// Link and store programs using the shaders at the given indices,
	/// and set up the basic uniforms.
	///
	/// For use in the constructor.
	unsafe fn setup_programs(&mut self, link_indices: &[(usize, usize)]) {
		let it = self.programs.iter_mut().zip(link_indices.iter());
		for (slot, (vsidx, fsidx)) in it {
			let program = link_program(
				self.vertex_shaders[*vsidx],
				self.fragment_shaders[*fsidx],
			)
			.unwrap();
			// At this point, program is guarented to
			// be valid. The following calls should never
			// raise an error.
			// Can be 0s, in that case the uniform are
			// understood as being absent.
			let model_to_camera_loc = gl::GetUniformLocation(
				program,
				MODEL_TO_CAMERA_NAME,
			);
			let normal_model_to_camera_loc = gl::GetUniformLocation(
				program,
				NORMAL_MODEL_TO_CAMERA_NAME,
			);
			let color_texture_loc =
				gl::GetUniformLocation(program, c_lit!(color));
			// Bind the sampler right away. Then, no need
			// the keep the uniform location around anymore.
			if color_texture_loc != -1 {
				gl::UseProgram(program);
				gl::Uniform1i(
					color_texture_loc,
					COLOR_TEXTURE_UNIT,
				);
			}
			*slot = Program {
				program,
				model_to_camera_loc,
				normal_model_to_camera_loc,
			};
		}
	}

	/// Load the textures from files.
	unsafe fn setup_textures<P>(&mut self, paths: &[P], buf: &mut Vec<u8>)
	where
		P: AsRef<Path>,
	{
		let it = self.color_textures.iter_mut().zip(paths.iter());
		for (slot, path) in it {
			let mut f = File::open(path)
				.expect("could not open texture file");
			buf.clear();
			f.read_to_end(buf)
				.expect("failed to read texture file");
			// Only care about the GL name.
			*slot = load_fbs_texture(buf).name;
		}
	}

	/// Instanciate a store.
	///
	/// Instanciate a store from
	/// - a list of vertex shader sources,
	/// - a list of fragment shader sources,
	/// - a list of couples of indices referencing the shaders used to
	/// link programs.
	/// - a list of paths to color texture files.
	// TODO: too complicated for a new() function?
	pub fn new<P>(
		vs_sources: &[&[u8]],
		fs_sources: &[&[u8]],
		program_link_indices: &[(usize, usize)],
		texture_paths: &[P],
	) -> RenderStateStore
	where
		P: AsRef<Path>,
	{
		let mut store = RenderStateStore {
			programs: [Program::NO_OP; 8],
			vertex_shaders: [0; 8],
			fragment_shaders: [0; 8],
			color_textures: [0; 8],
			sampler_2d: texture_sampler(),
		};

		debug_assert!(vs_sources.len() <= 8);
		debug_assert!(fs_sources.len() <= 8);
		debug_assert!(program_link_indices.len() <= 8);

		// Unique ~500KiB buffer for reading in all files.
		let mut buf = Vec::with_capacity(524_288);

		unsafe {
			store.setup_shaders(vs_sources, fs_sources);
			store.setup_programs(program_link_indices);
			store.setup_textures(texture_paths, &mut buf);
		}

		store
	}

	/// Get the key for a certain combinaison of state objects.
	// TODO: type-safe indices.
	pub fn key_for<'s>(
		&'s self,
		wind_ord: WindingOrder,
		p_idx: usize,
		tex_idx: usize,
	) -> RenderStateKey<'s> {
		debug_assert!(
			self.programs[p_idx].program != 0,
			"invalid program"
		);
		debug_assert!(
			self.color_textures[tex_idx] != 0,
			"invalid texture"
		);
		RenderStateKey::encoding(
			wind_ord,
			p_idx,
			tex_idx,
			PhantomData::<&'s RenderStateStore>,
		)
	}

	/// Set the complete rendering state corresponding to a key.
	///
	/// Returns a reference to the program that was just bound for
	/// uniform bindings.
	unsafe fn set_state_for<'s>(
		&'s self,
		key: RenderStateKey<'s>,
	) -> &Program {
		let (wind_ord, p_idx, tex_idx) = key.decode();
		// Winding order.
		match wind_ord {
			WindingOrder::CCW => gl::FrontFace(gl::CCW),
			WindingOrder::CW => gl::FrontFace(gl::CW),
		}
		// Program.
		let p = &self.programs[p_idx];
		gl::UseProgram(p.program);
		// Texture.
		let tex = self.color_textures[tex_idx];
		// TODO: I don't have to call this at every texture bind,
		// do I?
		gl::ActiveTexture(gl::TEXTURE0 + COLOR_TEXTURE_UNIT as GLenum);
		gl::BindTexture(gl::TEXTURE_2D, tex);
		p
	}

	/// Binds a uniform block to an UBO binding.
	///
	/// Finds the location then binds.
	///
	/// # Panics
	///
	/// Panics if the program index is
	/// invalid or points to an empty slot, or if the program does not
	/// have a corresponding uniform block.
	///
	///
	/// # Safety
	///
	/// Assumes the uniform buffer is already bound.
	// *TODO*: could return an error on a empty program slot instead
	// of panicking. Could return an error or silently ignore an absent
	// uniform block.
	pub unsafe fn block_binding<U, G>(&self, p_idx: usize)
	where
		U: Ubo,
		G: UboGet<U>,
	{
		let p = self.programs[p_idx].program;
		debug_assert!(p != 0, "empty program slot");
		let loc = gl::GetUniformBlockIndex(p, <G as UboGet<U>>::NAME);
		debug_assert!(
			loc != gl::INVALID_INDEX,
			"uniform block not in program",
		);
		gl::UniformBlockBinding(p, loc, <G as UboGet<U>>::BINDING);
	}

	/// Binds a material block to an UBO binding.
	///
	/// Finds the location then binds.
	///
	/// # Panics
	///
	/// Panics if the program index is invalid or points to an
	/// empty slot, or if the program does not have a corresponding
	/// uniform block.
	///
	///
	/// # Safety
	///
	/// Assumes the uniform buffer is already bound.
	// *TODO*: could return an error on a empty program slot instead
	// of panicking. Could return an error or silently ignore an absent
	// uniform block.
	pub unsafe fn mat_binding<U, G>(&self, p_idx: usize)
	where
		U: MaterialUbo,
		G: MaterialUboField<U>,
	{
		let p = self.programs[p_idx].program;
		debug_assert!(p != 0, "empty program slot");
		let loc = gl::GetUniformBlockIndex(
			p,
			<G as MaterialUboField<U>>::NAME,
		);
		let cstr = std::ffi::CStr::from_ptr::<'static>(
			<G as MaterialUboField<U>>::NAME,
		);
		dbg!(cstr);
		dbg!(<G as MaterialUboField<U>>::BINDING);
		dbg!(loc);
		debug_assert!(
			loc != gl::INVALID_INDEX,
			"uniform block not in program",
		);
		gl::UniformBlockBinding(
			p,
			loc,
			<G as MaterialUboField<U>>::BINDING,
		);
	}
}

impl Drop for RenderStateStore {
	/// Flag the programs and shaders for deletion.
	fn drop(&mut self) {
		// Flag the programs for deletion. Assumes there's only
		// valid GL programs or 0s (which are silently ignored). The
		// shaders are automatically detached.
		for p in self.programs.iter() {
			unsafe { gl::DeleteProgram(p.program) };
		}
		// Flag the shaders for deletion. Since they were detached
		// from the programs, they should be deleted.
		for s in self
			.vertex_shaders
			.iter()
			.chain(self.fragment_shaders.iter())
		{
			unsafe { gl::DeleteShader(*s) };
		}

		unsafe {
			gl::DeleteTextures(8, self.color_textures.as_ptr());
		}

		unsafe {
			gl::DeleteSamplers(1, &self.sampler_2d);
		}
		glerr!();
	}
}

pub type RenderStateKeyBits = u32;

/// Encodes the rendering state of a node as a bitmask.
///
/// The indices of all parts of the rendering state (e.g. program) in the
/// state store are found in subsets of the mask. This allows easy sorting.
/// Note that a key is not linked in a type-safe way to the store it
/// came from.
#[derive(Clone, Copy, PartialEq, Eq)]
pub struct RenderStateKey<'s> {
	key: RenderStateKeyBits,
	_store: PhantomData<&'s RenderStateStore>,
}

impl<'s> RenderStateKey<'s> {
	const WINDING_MASK: RenderStateKeyBits = 0b0_0000_0001;
	const PROGRAM_MASK: RenderStateKeyBits = 0b0_0000_1110;
	const TEXTURE_MASK: RenderStateKeyBits = 0b0_0111_0000;
	/// Must represent a state impossible to construct.
	const NO_STATE: RenderStateKey<'s> = RenderStateKey {
		key: RenderStateKeyBits::MAX,
		_store: PhantomData::<&'s RenderStateStore>,
	};

	/// Return the maximum index allowed by a bit mask.
	///
	/// Assumes that the size of RenderStateKeyBits is not greater than
	/// an usize.
	#[inline]
	const fn max_idx(mask: RenderStateKeyBits) -> usize {
		(1 << mask.count_ones()) as usize
	}

	/// Return the bit pattern corresponding to given index and mask.
	///
	/// Assumes the index is smaller than the greatest index encodable
	/// by the mask.
	///
	// HUGE ASS WARNING: doesn't this assume the endianess of `idx`?
	// Might be worth using an external crate for this bitmask stuff.
	#[inline]
	fn masked_bits(
		idx: usize,
		mask: RenderStateKeyBits,
	) -> RenderStateKeyBits {
		(idx as RenderStateKeyBits & (mask >> mask.trailing_zeros()))
			<< mask.trailing_zeros()
	}

	/// Return the integer corresponding to the given bitfield and mask.
	#[inline]
	fn unmask_bits(
		bits: RenderStateKeyBits,
		mask: RenderStateKeyBits,
	) -> usize {
		((bits & mask) >> mask.trailing_zeros()) as usize
	}

	/// Return a key encoding the given program and texture index.
	///
	/// Uses a phantom marker to a store to constrain the key's lifetime.
	fn encoding(
		wind_ord: WindingOrder,
		p_idx: usize,
		tex_idx: usize,
		store: PhantomData<&'s RenderStateStore>,
	) -> RenderStateKey<'s> {
		debug_assert!(
			p_idx < RenderStateKey::max_idx(
				RenderStateKey::PROGRAM_MASK
			)
		);
		debug_assert!(
			tex_idx < RenderStateKey::max_idx(
				RenderStateKey::TEXTURE_MASK
			)
		);
		// Must be coherent with the match in RenderStateKey::decode.
		let wind_int = match wind_ord {
			WindingOrder::CCW => 0,
			WindingOrder::CW => 1,
		};
		let bits = RenderStateKey::masked_bits(
			wind_int,
			RenderStateKey::WINDING_MASK,
		) | RenderStateKey::masked_bits(
			p_idx,
			RenderStateKey::PROGRAM_MASK,
		) | RenderStateKey::masked_bits(
			tex_idx,
			RenderStateKey::TEXTURE_MASK,
		);
		RenderStateKey {
			key: bits,
			_store: store,
		}
	}

	/// Decode the key into a explicit render state.
	///
	/// Winding order, program index and texture index.
	fn decode(&self) -> (WindingOrder, usize, usize) {
		let wind_int = RenderStateKey::unmask_bits(
			self.key,
			RenderStateKey::WINDING_MASK,
		);
		let wind_ord = match wind_int {
			0 => WindingOrder::CCW,
			1 => WindingOrder::CW,
			_ => unreachable!(),
		};
		let p_idx = RenderStateKey::unmask_bits(
			self.key,
			RenderStateKey::PROGRAM_MASK,
		);
		let tex_idx = RenderStateKey::unmask_bits(
			self.key,
			RenderStateKey::TEXTURE_MASK,
		);
		(wind_ord, p_idx, tex_idx)
	}
}

/// Translation, scale and rotation from an object's space to world.
// TODO: Copy is convenient but dunno if there's a performance hit.
#[derive(Clone, Copy)]
pub struct ModelWorldTransform {
	pub pos: glam::Vec3,
	pub scale: glam::Vec3,
	pub quat: glam::Quat,
}

impl ModelWorldTransform {
	/// Matrix representation of the transform.
	fn matrix(&self) -> glam::Mat4 {
		glam::Mat4::from_scale_rotation_translation(
			self.scale, self.quat, self.pos,
		)
	}
}

/// Necessary information to draw a model.
///
/// Encodes the state relative to a [`RenderStateStore`], knows what draw
/// commands to issue, and has its own [`ModelWorldTransform`].
pub struct RenderNode<'s, 'm> {
	transform: ModelWorldTransform,
	state_key: RenderStateKey<'s>,
	mat_key: MaterialUboKey,
	model_cmds: &'m [DrawCommand],
}

impl<'s, 'm> RenderNode<'s, 'm> {
	pub fn transform_mut(&mut self) -> &mut ModelWorldTransform {
		&mut self.transform
	}
}

/// A list of [`RenderNode`]s along the necessary draw state and data.
///
/// This struct holds a list of [`RenderNode`] together with its targeted
/// [`RenderStateStore`] and [`ModelStore`]. When the queue is used to create
/// nodes ([`RenderQueue::push_node`]) and draw them
/// ([`RenderQueue::draw_nodes`]), everything will be coherent.
// TODO: could consider not using references. Doubt I'd share stores or
// ubo between multiple queue.
pub struct RenderQueue<'s, 'm, 'u, 't, U: Ubo, M: MaterialUbo> {
	nodes: Vec<RenderNode<'s, 'm>>,
	state_store: &'s RenderStateStore,
	model_store: &'m ModelStore,
	ubo: &'u mut U,
	mat_ubo: &'t mut M,
}

impl<'s, 'm, 'u, 't, U: Ubo, M: MaterialUbo> RenderQueue<'s, 'm, 'u, 't, U, M> {
	/// Create an empty queue with the given capacity.
	///
	/// Create an empty queue with the given capacity. and coherent with
	/// the given stores and ubo.
	pub fn with_capacity(
		cap: usize,
		state_store: &'s RenderStateStore,
		model_store: &'m ModelStore,
		ubo: &'u mut U,
		mat_ubo: &'t mut M,
	) -> RenderQueue<'s, 'm, 'u, 't, U, M> {
		RenderQueue::<'s, 'm, 'u, 't, U, M> {
			nodes: Vec::with_capacity(cap),
			state_store,
			model_store,
			ubo,
			mat_ubo,
		}
	}

	/// Get the key for a certain combinaison of state objects.
	// TODO: type-safe indices.
	pub fn key_for(
		&self,
		wind_ord: WindingOrder,
		p_idx: usize,
		tex_idx: usize,
	) -> RenderStateKey<'s> {
		self.state_store.key_for(wind_ord, p_idx, tex_idx)
	}

	/// Get the material key for a certain combinaison of materials.
	///
	/// Leave unused materials as `None`.
	pub fn mat_key_for(&self, indices: M::StoreIdx) -> MaterialUboKey {
		self.mat_ubo.key_for(indices)
	}

	/// Modify the transform of the node at the given index.
	///
	/// Modify the transform of the node at the given index using
	/// a closure.
	pub fn set_transform_with<F>(&mut self, idx: usize, transform_fn: F)
	where
		F: FnOnce(&mut ModelWorldTransform),
	{
		transform_fn(&mut self.nodes[idx].transform);
	}

	/// Draw each node in the queue with the given camera.
	///
	/// Will not clear the queue. The necessary GL state is bound first.
	// TODO: sort the queue.
	pub fn draw_nodes(&self, camera_look_at: &glam::Mat4) {
		// Set state.
		unsafe {
			gl::BindVertexArray(self.model_store.vao);
			gl::BindBuffer(
				gl::ARRAY_BUFFER,
				self.model_store.buffers[0],
			);
			gl::BindBuffer(
				gl::ELEMENT_ARRAY_BUFFER,
				self.model_store.buffers[1],
			);
			gl::BindBuffer(gl::UNIFORM_BUFFER, self.ubo.gl_name());
			gl::BindSampler(
				COLOR_TEXTURE_UNIT as GLuint,
				self.state_store.sampler_2d,
			);
		}

		unsafe {
			self.ubo.buffer_data();
		}
		let mut previous_state = RenderStateKey::NO_STATE;
		let mut previous_mat = MaterialUboKey::NO_MATERIAL;
		// Using a copy of the current program rather than a reference.
		// TODO: Might be worth implementing Copy for Program.
		let mut current_program = Program::NO_OP;
		// For each node, bind the program if needed, and forward
		// the uniform data. Then, draw the model.
		// TODO: justify that the unsafe is fine.
		// TODO: holy indentation batman
		unsafe {
			gl::BindBuffer(
				gl::UNIFORM_BUFFER,
				self.mat_ubo.gl_name(),
			);
			for node in self.nodes.iter() {
				if node.state_key != previous_state {
					// For now, bind the state in full if
					// anything changes.
					current_program = Clone::clone(
						self.state_store.set_state_for(
							node.state_key,
						),
					)
				}
				if node.mat_key != previous_mat {
					// For now, bind the material in full if
					// anything changes.
					self.mat_ubo.bind_key(node.mat_key);
				}
				current_program
					.update_model_to_camera_uniforms(
						&node.transform,
						camera_look_at,
					);
				for cmd in node.model_cmds.iter() {
					cmd.draw();
				}
				previous_state = node.state_key;
				previous_mat = node.mat_key;
			}
		}
	}

	/// Constructs a [`RenderNode`] in the queue.
	///
	/// Constructs a [`RenderNode`] in the queue with the given state
	/// key. Returns the index of the new node.
	///
	/// # Safety
	///
	/// The key must be valid for this renderer.
	pub unsafe fn push_node_key(
		&mut self,
		transform: ModelWorldTransform,
		model_idx: usize,
		state_key: RenderStateKey<'s>,
		mat_key: MaterialUboKey,
	) -> usize {
		self.nodes.push(RenderNode::<'s, 'm> {
			transform,
			state_key,
			mat_key,
			model_cmds: self.model_store.cmds_for(model_idx),
		});
		self.nodes.len() - 1
	}

	/// Constructs a [`RenderNode`] in the queue.
	///
	/// Constructs a [`RenderNode`] in the queue using the model,
	/// program and color texture at the given indices.
	///
	/// # Panics
	///
	/// Assumes the key is valid: use [`StateStore::key_for`] to be sure.
	// TODO: type-safe indices.
	pub fn push_node(
		&mut self,
		transform: ModelWorldTransform,
		model_idx: usize,
		state_key: RenderStateKey<'s>,
		mat_key: MaterialUboKey,
	) {
		unsafe {
			self.push_node_key(
				transform, model_idx, state_key, mat_key,
			);
		}
	}

	/// Clear the queue, removing all nodes.
	pub fn clear(&mut self) {
		self.nodes.clear();
	}

	/// Returns the [`Ubo`] of the queue for manipulation.
	///
	/// Might end up deprecated for higher-level functions.
	pub fn ubo(&mut self) -> &mut U {
		self.ubo
	}

	/// Sets a node's material.
	// TODO: panics section
	pub fn set_mat_key(&mut self, idx: usize, mat_key: MaterialUboKey) {
		self.nodes[idx].mat_key = mat_key
	}
}

/// Coordinates of individual "images" in a texture atlas.
#[derive(Copy, Clone, Debug)]
pub struct AtlasRectangle {
	pub lower_left: UVec2,
	pub dims: UVec2,
}

impl AtlasRectangle {
	const NONE: AtlasRectangle = AtlasRectangle {
		lower_left: const_uvec2!([0, 0]),
		dims: const_uvec2!([0, 0]),
	};

	/// Check if this rectangle overlaps with another one.
	pub fn overlaps(&self, other: AtlasRectangle) -> bool {
		self.lower_left.x < other.lower_left.x + other.dims.x
			&& other.lower_left.x < self.lower_left.x + self.dims.x
			&& self.lower_left.y < other.lower_left.y + other.dims.y
			&& other.lower_left.y < self.lower_left.y + self.dims.y
	}

	/// Check if this rectangle overextends an atlas.
	pub fn overextends(&self, width: usize, height: usize) -> bool {
		self.lower_left.x + self.dims.x > width as u32
			|| self.lower_left.y + self.dims.y > height as u32
	}
}

impl From<FbsRectangle> for AtlasRectangle {
	fn from(fbs: FbsRectangle) -> AtlasRectangle {
		let lower_left =
			UVec2::new(fbs.lower_left().x(), fbs.lower_left().y());
		let dims = UVec2::new(fbs.dims().x(), fbs.dims().y());
		AtlasRectangle { lower_left, dims }
	}
}

/// Textured quad transformation.
#[derive(Clone, Copy, Debug)]
pub struct QuadTransform {
	pub pos: Vec2,
	pub scale: Vec2,
	pub rot_angle: f32,
}

impl QuadTransform {
	pub const IDENTITY: QuadTransform = QuadTransform {
		pos: Vec2::ZERO,
		scale: Vec2::ONE,
		rot_angle: 0.0,
	};
}

/// A collection of rectangle images in a single texture.
///
/// Used to compute the UV attributes when rendering textured quads.
#[derive(Debug)]
struct TextureAtlas {
	name: GLuint,
	width: u32,
	height: u32,
	rects: [AtlasRectangle; Self::RECTANGLE_MAX],
}

impl TextureAtlas {
	const RECTANGLE_MAX: usize = 16;
	const NONE: TextureAtlas = TextureAtlas {
		name: 0,
		width: 0,
		height: 0,
		rects: [AtlasRectangle::NONE; Self::RECTANGLE_MAX],
	};
}

#[derive(Debug)]
struct Section {
	first: GLint,
	count: GLsizei,
	z: GLfloat,
	atlas_idx: usize,
	overlay_color: Vec4,
}

impl Section {
	const NONE: Section = Section {
		first: 0,
		count: 0,
		z: 0.0,
		atlas_idx: 0,
		overlay_color: Vec4::ZERO,
	};
}

pub struct SectionArg {
	pub capacity: usize,
	pub z: GLfloat,
	pub texture_idx: usize,
	pub overlay_color: Vec4,
}

/// Renders textured 2D quads.
///
/// This struct holds both the vertex data (through a memory-mapped buffer)
/// and the list of possible rendering states (right now, just texture
/// bindings).
///
/// In contrast to the 3D renderering ([`RenderQueue`] and associated),
/// the vertex data buffer is not static. Instead, it is fed dynamically
/// from the CPU (using the [`QuadTransform`] and [`AtlasRectangle`] structs).
// Can I feed the buffer in parallel?
// `firsts`, `lens` are in quad "units": 1 means 4 vertices, or 16 floats.
// `buf_size` is in the machine units expected by glMapBufferRange and such.
// TODO: enforce those units through the type system instead. Make sure
// the alignments are correct though.
pub struct TexQuadRenderer {
	vao: GLuint,
	vbo: GLuint,
	vs: GLuint,
	fs: GLuint,
	program: GLuint,
	image_to_clip_loc: GLint,
	z_loc: GLint,
	overlay_color_loc: GLint,
	sampler: GLuint,
	memmapped: NonNull<GLvoid>,
	buf_size: GLsizeiptr,
	atlases: [TextureAtlas; Self::SECTION_MAX],
	sections: [Section; Self::SECTION_MAX],
}

impl TexQuadRenderer {
	const SECTION_MAX: usize = 8;
	const VERTEX_PER_QUAD: usize = 6;
	// 2*6 f32s for pos, 2*6 f32s for uvs.
	const FLOATS_PER_QUAD: usize = 2 * 2 * Self::VERTEX_PER_QUAD;
	const BYTES_PER_QUAD: usize =
		Self::FLOATS_PER_QUAD * mem::size_of::<GLfloat>();
	// Interleaved attributes: posx posy u v | posx posy u v | ...
	const ATTRIBUTE_STRIDE: usize = mem::size_of::<GLfloat>() * 4;
	const UV_OFFSET: usize = mem::size_of::<GLfloat>() * 2;

	/// Vertex positions for the unit quad.
	const QUAD_POS: [Vec2; Self::VERTEX_PER_QUAD] = [
		const_vec2!([-0.5, 0.5]),
		const_vec2!([-0.5, -0.5]),
		const_vec2!([0.5, 0.5]),
		const_vec2!([0.5, 0.5]),
		const_vec2!([-0.5, -0.5]),
		const_vec2!([0.5, -0.5]),
	];

	/// Vertex UVs for the unit quad.
	const QUAD_UV: [Vec2; Self::VERTEX_PER_QUAD] = [
		const_vec2!([0.0, 1.0]),
		const_vec2!([0.0, 0.0]),
		const_vec2!([1.0, 1.0]),
		const_vec2!([1.0, 1.0]),
		const_vec2!([0.0, 0.0]),
		const_vec2!([1.0, 0.0]),
	];

	// `capacities` is the number of quads allocated for each section.
	/// Crate a renderer with the given capacities for each section
	/// and that load and store the texture from the given paths.
	///
	/// The `capacities` parameter is the list of number of quads
	/// allocated for each section (and not the number of vertices,
	/// floats or bytes). Only the first [`TexQuadRenderer::SECTION_MAX`]
	/// sizes in this list will be used, the rest is silently discarded.
	// This silent discard might bite me in the rear later. Consider either
	// an assert or printing a warning.
	pub fn new<P>(
		atlases_paths: &[P],
		sec_args: &[SectionArg],
	) -> TexQuadRenderer
	where
		P: AsRef<Path>,
	{
		// Setup program.
		let (vs, fs, program) = unsafe {
			let vs = compile_vertex_shader(include_bytes!(
				"shaders/texquad.vs"
			))
			.unwrap();
			let fs = compile_fragment_shader(include_bytes!(
				"shaders/texquad.fs"
			))
			.unwrap();
			(vs, fs, link_program(vs, fs).unwrap())
		};
		let color_texture_loc = unsafe {
			gl::UseProgram(program);
			gl::GetUniformLocation(program, c_lit!(color))
		};
		// Bind the sampler right away. Then, no need
		// the keep the uniform location around anymore.
		debug_assert!(
			color_texture_loc != -1,
			"couldn't get the texquad sampler location"
		);
		unsafe {
			gl::Uniform1i(color_texture_loc, COLOR_TEXTURE_UNIT);
		}

		let image_to_clip_loc = unsafe {
			gl::GetUniformLocation(program, c_lit!(image_to_clip))
		};
		debug_assert!(
			image_to_clip_loc != -1,
			"couldn't get the texquad image-to-clip location"
		);
		let z_loc =
			unsafe { gl::GetUniformLocation(program, c_lit!(z)) };
		debug_assert!(
			z_loc != -1,
			"couldn't get the texquad z-level location"
		);
		let overlay_color_loc = unsafe {
			gl::GetUniformLocation(program, c_lit!(overlay_color))
		};
		debug_assert!(
			overlay_color_loc != -1,
			"couldn't get the color overlay location",
		);

		// Setup textures.
		let mut buf = Vec::with_capacity(4096);
		let mut atlases = [TextureAtlas::NONE; Self::SECTION_MAX];
		let it = atlases.iter_mut().zip(atlases_paths.iter());
		for (atlas, path) in it {
			let mut f = File::open(path)
				.expect("could not open texture file");
			buf.clear();
			f.read_to_end(&mut buf)
				.expect("failed to read texture file");
			*atlas = load_fbs_texture(&buf);
		}

		// Setup sections and compute vertex buffer size.
		let mut sections = [Section::NONE; Self::SECTION_MAX];
		let mut partial_sum = 0;
		let mut last_init_idx = 0;
		for (sec, arg) in sections.iter_mut().zip(sec_args.iter()) {
			sec.first = partial_sum;
			sec.count = 0;
			sec.z = arg.z;
			sec.overlay_color = arg.overlay_color;

			partial_sum += arg.capacity as GLint;
			last_init_idx += 1;
		}
		for sec in sections[last_init_idx..].iter_mut() {
			sec.first = partial_sum;
		}

		let buf_size = (Self::BYTES_PER_QUAD as GLint * partial_sum)
			as GLsizeiptr;

		let mut vbo = 0;
		let mut vao = 0;
		let memmapped = NonNull::new(unsafe {
			// TODO: Error-checking.
			gl::CreateBuffers(1, &mut vbo);
			gl::GenVertexArrays(1, &mut vao);

			gl::BindVertexArray(vao);
			gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
			gl::BufferData(
				gl::ARRAY_BUFFER,
				buf_size,
				ptr::null(),
				gl::DYNAMIC_DRAW,
			);
			// Assume pos and uv data are the same size.
			// Position attribute (0).
			gl::EnableVertexAttribArray(0);
			gl::VertexAttribPointer(
				0,
				2,
				gl::FLOAT,
				gl::FALSE,
				Self::ATTRIBUTE_STRIDE as GLsizei,
				0 as *const GLvoid,
			);
			// UV attribute (2).
			gl::EnableVertexAttribArray(2);
			gl::VertexAttribPointer(
				2,
				2,
				gl::FLOAT,
				gl::FALSE,
				Self::ATTRIBUTE_STRIDE as GLsizei,
				Self::UV_OFFSET as *const GLvoid,
			);

			gl::MapBufferRange(
				gl::ARRAY_BUFFER,
				0,
				buf_size,
				gl::MAP_WRITE_BIT,
			)
		})
		.expect("gl::MapBuffer returned null");
		glerr!();

		TexQuadRenderer {
			vao,
			vbo,
			vs,
			fs,
			program,
			image_to_clip_loc,
			z_loc,
			overlay_color_loc,
			sampler: texture_sampler(),
			memmapped,
			buf_size,
			atlases,
			sections,
		}
	}

	/// Write the transformed quad vertices.
	///
	/// Write the transformed quad vertices attributes in the 16-float
	/// slice `dst`, to be interleaved with the UV coordinates
	/// ([`TexQuadRenderer::gl_uv_data`]).
	fn gl_pos_data(
		transform: QuadTransform,
		rect: AtlasRectangle,
		dst: &mut [GLfloat],
	) {
		// Trusting glam to generate faster algebra than I can code.
		let mat = Mat3::from_scale_angle_translation(
			transform.scale * rect.dims.as_f32(),
			transform.rot_angle,
			transform.pos,
		);
		// 4 f32s per vertex.
		let it = Self::QUAD_POS.iter().zip(dst.chunks_exact_mut(4));
		for (v, d) in it {
			mat.transform_point2(*v)
				.write_to_slice_unaligned(&mut d[0..2]);
		}
	}

	/// Write the rectangle UV coordinates.
	///
	/// Write the UV coordinates of the vertices of this rectangle in
	/// the 24-float slice `dst`, to be interleaved with the position
	/// coordinates ([`TexQuadRenderer::gl_pos_data`]). Needs the atlas'
	/// width and height for normalization.
	fn gl_uv_data(
		rect: AtlasRectangle,
		atlas_width: u32,
		atlas_height: u32,
		dst: &mut [GLfloat],
	) {
		let offset = Vec2::new(
			rect.lower_left.x as f32 / atlas_width as f32,
			rect.lower_left.y as f32 / atlas_height as f32,
		);
		let scale = Vec2::new(
			rect.dims.x as f32 / atlas_width as f32,
			rect.dims.y as f32 / atlas_height as f32,
		);
		// 4 f32s per vertex.
		let it = Self::QUAD_UV.iter().zip(dst.chunks_exact_mut(4));
		for (v, d) in it {
			(*v * scale + offset)
				.write_to_slice_unaligned(&mut d[2..4]);
		}
	}
	/// Get the current offset to the section's head for a `*f32` pointer.
	fn f32_offset_for(&self, sec_idx: usize) -> isize {
		((self.sections[sec_idx].first + self.sections[sec_idx].count)
			* Self::FLOATS_PER_QUAD as GLint) as isize
	}

	fn debug_assert_section_full(&self, sec_idx: usize) {
		#[cfg(build = "debug")]
		self.debug_assert_section_full_inner(sec_idx)
	}

	#[cfg(build = "debug")]
	fn debug_assert_section_full_inner(&self, sec_idx: usize) {
		let sec = &self.sections[sec_idx];
		let sec_current_idx = sec.first + sec.count;
		let next_sec_start_idx = self.sections.get(sec_idx + 1).map_or(
			(self.buf_size as usize / Self::BYTES_PER_QUAD)
				as GLint,
			|next| next.first,
		);
		assert!(
			sec_current_idx < next_sec_start_idx,
			"section is full"
		);
	}

	/// Push a single quad into the rendering list.
	///
	/// # Safety
	///
	/// Assumes the VBO (and VAO?) is bound... probably, not exactly
	/// clear with the mapping. Won't hurt to assume that in any case.
	///
	/// # Panics
	///
	/// In debug builds, panics if the section is full.
	pub unsafe fn push_to_section(
		&mut self,
		sec_idx: usize,
		transform: QuadTransform,
		rect_idx: usize,
	) {
		self.debug_assert_section_full(sec_idx);

		let offset = self.f32_offset_for(sec_idx);
		let mut vertex_data: &mut [f32] = slice::from_raw_parts_mut(
			(self.memmapped.as_ptr() as *mut GLfloat)
				.offset(offset),
			Self::FLOATS_PER_QUAD,
		);
		let sec = &mut self.sections[sec_idx];
		let atlas = &self.atlases[sec.atlas_idx];
		let rect = atlas.rects[rect_idx];
		Self::gl_pos_data(transform, rect, &mut vertex_data);
		Self::gl_uv_data(
			rect,
			atlas.width,
			atlas.height,
			&mut vertex_data,
		);

		sec.count += 1;
	}

	pub fn push_laser_to_section(
		&mut self,
		sec_idx: usize,
		rect_idx: usize,
		midpoint: Vec2,
		laser_length: f32,
		uv_y_offset: f32,
		angle: f32,
	) {
		self.debug_assert_section_full(sec_idx);

		let offset = self.f32_offset_for(sec_idx);
		let mut vertex_data: &mut [f32] = unsafe {
			slice::from_raw_parts_mut(
				(self.memmapped.as_ptr() as *mut GLfloat)
					.offset(offset),
				Self::FLOATS_PER_QUAD,
			)
		};
		let sec = &mut self.sections[sec_idx];
		let atlas = &self.atlases[sec.atlas_idx];
		let rect = atlas.rects[rect_idx];

		// Position
		let transform = QuadTransform {
			pos: midpoint,
			scale: Vec2::new(
				1.0,
				laser_length / rect.dims.y as f32,
			),
			rot_angle: angle,
		};
		Self::gl_pos_data(transform, rect, &mut vertex_data);

		// UV.
		let offset = Vec2::new(
			rect.lower_left.x as f32 / atlas.width as f32,
			(rect.lower_left.y as f32 - uv_y_offset)
				/ atlas.height as f32,
		);
		let scale = Vec2::new(
			rect.dims.x as f32 / atlas.width as f32,
			laser_length / atlas.height as f32,
		);
		// 4 f32s per vertex.
		let it = Self::QUAD_UV
			.iter()
			.zip(vertex_data.chunks_exact_mut(4));
		for (v, d) in it {
			(*v * scale + offset)
				.write_to_slice_unaligned(&mut d[2..4]);
		}

		sec.count += 1;
	}

	/// Get a rectangle in the atlas of a given section.
	pub fn get_rect_for(
		&self,
		sec_idx: usize,
		rect_idx: usize,
	) -> AtlasRectangle {
		let atlas_idx = self.sections[sec_idx].atlas_idx;
		self.atlases[atlas_idx].rects[rect_idx]
	}

	/// Clear all sections.
	///
	/// This just sets the lengths of all section to 0, meaning a new
	/// draw call will ignore the data that is still left in the buffer if
	/// it's not overwritten.
	pub fn clear(&mut self) {
		for sec in self.sections.iter_mut() {
			sec.count = 0;
		}
	}

	/// Draw every quad.
	///
	/// The necessary GL state is bound first. The buffer is not cleared
	/// after drawing.
	pub fn draw(&mut self, image_to_clip: Mat4) {
		// Set state.
		unsafe {
			gl::UseProgram(self.program);
			gl::BindVertexArray(self.vao);
			gl::BindBuffer(gl::ARRAY_BUFFER, self.vbo);
			gl::BindSampler(
				COLOR_TEXTURE_UNIT as GLuint,
				self.sampler,
			);
			// Assuming only the color texture will be bound.
			gl::ActiveTexture(
				gl::TEXTURE0 + COLOR_TEXTURE_UNIT as GLenum,
			);
			// Must restore the front face, as this is susceptible
			// to be changed by a mesh renderer.
			gl::FrontFace(gl::CCW);
		}

		// For now, assume there will only be one image-to-clip for all sections.
		unsafe {
			gl::UniformMatrix4fv(
				self.image_to_clip_loc,
				1,
				gl::FALSE,
				image_to_clip.as_ref() as *const GLfloat,
			);
		}

		// Unmap the buffer. Since GL_MAP_FLUSH_EXPLICIT_BIT is
		// not used when mapping, this also flush the writes.
		unsafe {
			if gl::UnmapBuffer(gl::ARRAY_BUFFER) != gl::TRUE {
				panic!("error occured during glUnmapBuffer");
			}
		}

		// Draw.
		let it = self.sections.iter().filter(|sec| sec.count > 0);
		for sec in it {
			unsafe {
				// Section's state.
				gl::Uniform1f(self.z_loc, sec.z);
				gl::Uniform4f(
					self.overlay_color_loc,
					sec.overlay_color.x,
					sec.overlay_color.y,
					sec.overlay_color.z,
					sec.overlay_color.w,
				);
				// TODO: Pretty sure this assumes some stuff
				// about texture bindings, like a previous
				// call to glActiveTexture.
				gl::BindTexture(
					gl::TEXTURE_2D,
					self.atlases[sec.atlas_idx].name,
				);

				let first = sec.first
					* Self::VERTEX_PER_QUAD as GLint;
				let count = sec.count
					* Self::VERTEX_PER_QUAD as GLsizei;
				gl::DrawArrays(gl::TRIANGLES, first, count);
			}
		}

		// Remap it.
		unsafe {
			self.memmapped = NonNull::new(gl::MapBufferRange(
				gl::ARRAY_BUFFER,
				0,
				self.buf_size,
				gl::MAP_WRITE_BIT,
			))
			.expect("glMapBufferRange returned null");
		}
	}
}

impl Drop for TexQuadRenderer {
	fn drop(&mut self) {
		unsafe {
			gl::DeleteProgram(self.program);
			gl::DeleteShader(self.vs);
			gl::DeleteShader(self.fs);
			for atlas in self.atlases.iter() {
				gl::DeleteTextures(1, &atlas.name as *const _)
			}
			gl::DeleteVertexArrays(1, &self.vao);
			gl::DeleteBuffers(1, &self.vbo as *const _);
			gl::DeleteSamplers(1, &self.sampler);
		}
	}
}

/// Creates a texture sampler.
///
/// Nearest mag/min. Repeat along the first two dimenions.
fn texture_sampler() -> GLuint {
	unsafe {
		let mut sampler = 0;
		gl::GenSamplers(1, &mut sampler);
		gl::SamplerParameteri(
			sampler,
			gl::TEXTURE_MAG_FILTER,
			gl::NEAREST as GLint,
		);
		gl::SamplerParameteri(
			sampler,
			gl::TEXTURE_MIN_FILTER,
			gl::NEAREST as GLint,
		);
		gl::SamplerParameteri(
			sampler,
			gl::TEXTURE_WRAP_S,
			gl::REPEAT as GLint,
		);
		gl::SamplerParameteri(
			sampler,
			gl::TEXTURE_WRAP_T,
			gl::REPEAT as GLint,
		);
		sampler
	}
}
