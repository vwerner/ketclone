//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

#version 330
layout(location = 0) in vec2 vertex_position;
layout(location = 2) in vec2 vertex_uv;
smooth out vec2 vs_uv;
flat out vec4 vs_overlay_color;

uniform vec4 overlay_color;
uniform mat4 image_to_clip;
uniform float z;

void main() {
	gl_Position = image_to_clip * vec4(vertex_position, z, 1.0);
	vs_uv = vertex_uv;
	vs_overlay_color = overlay_color;
}
