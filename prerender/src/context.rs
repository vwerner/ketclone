//    Copyright 2020 The glutin contributors
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

// Mostly a copy-paste of
// https://github.com/rust-windowing/glutin/blob/v0.26.0/glutin_examples/examples/headless.rs
// See the copyright and license notice above and the file LICENSE_glutin.
// Changes made:
//     - extract the context initialization in a new Context struct
//       (wrapping glutin::Context), dropping the main function and the
//       dependency on the support module

//! Headless OpenGL context initialization using glutin.
//!
//! Mostly a copy-paste of
//! https://github.com/rust-windowing/glutin/blob/v0.26.0/glutin_examples/examples/headless.rs

use gfx::gl;
use glutin::{
	dpi::PhysicalSize, event_loop::EventLoop, Api, ContextBuilder,
	ContextCurrentState, CreationError, GlProfile, GlRequest, NotCurrent,
	PossiblyCurrent,
};

pub struct Context {
	_ctx: glutin::Context<PossiblyCurrent>,
	_el: EventLoop<()>,
}

impl Context {
	pub fn create() -> Result<Context, CreationError> {
		let cb = glutin::ContextBuilder::new()
			.with_gl_profile(GlProfile::Core)
			.with_gl(GlRequest::Specific(Api::OpenGl, (3, 3)));
		let (headless_ctx, _el) = build_context(cb).unwrap();
		let headless_ctx =
			unsafe { headless_ctx.make_current().unwrap() };
		gl::load_with(|ptr| {
			headless_ctx.get_proc_address(ptr) as *const _
		});

		// Initialize some global parameters.
		unsafe {
			gl::ClearColor(0.6, 0.2, 0.8, 0.0);
			gl::Enable(gl::DEPTH_TEST);
			gl::DepthFunc(gl::LESS);
			gl::Enable(gl::CULL_FACE);
			gl::FrontFace(gl::CCW);
			gl::CullFace(gl::BACK);
		}

		Ok(Context {
			_ctx: headless_ctx,
			_el,
		})
	}
}

#[cfg(target_os = "linux")]
fn build_context_surfaceless<T: ContextCurrentState>(
	cb: ContextBuilder<T>,
	el: &EventLoop<()>,
) -> Result<glutin::Context<NotCurrent>, CreationError> {
	use glutin::platform::unix::HeadlessContextExt;
	cb.build_surfaceless(el)
}

fn build_context_headless<T1: ContextCurrentState>(
	cb: ContextBuilder<T1>,
	el: &EventLoop<()>,
) -> Result<glutin::Context<NotCurrent>, CreationError> {
	let size_one = PhysicalSize::new(1, 1);
	cb.build_headless(el, size_one)
}

#[cfg(target_os = "linux")]
fn build_context_osmesa<T1: ContextCurrentState>(
	cb: ContextBuilder<T1>,
) -> Result<glutin::Context<NotCurrent>, CreationError> {
	use glutin::platform::unix::HeadlessContextExt;
	let size_one = PhysicalSize::new(1, 1);
	cb.build_osmesa(size_one)
}

fn build_context<T: ContextCurrentState>(
	cb: ContextBuilder<T>,
) -> Result<(glutin::Context<NotCurrent>, EventLoop<()>), [CreationError; 3]> {
	// "On unix operating systems, you should always try for surfaceless
	// first, and if that does not work, headless (pbuffers), and if
	// that too fails, finally osmesa". From Glutin's headless example.
	let el = EventLoop::new();

	println!("Trying surfaceless GL context.");
	let err1 = match build_context_surfaceless(cb.clone(), &el) {
		Ok(ctx) => return Ok((ctx, el)),
		Err(err) => err,
	};

	println!("Trying headless GL context.");
	let err2 = match build_context_headless(cb.clone(), &el) {
		Ok(ctx) => return Ok((ctx, el)),
		Err(err) => err,
	};

	println!("Trying osmesa GL context.");
	let err3 = match build_context_osmesa(cb) {
		Ok(ctx) => return Ok((ctx, el)),
		Err(err) => err,
	};

	Err([err1, err2, err3])
}

#[cfg(not(target_os = "linux"))]
fn build_context<T: ContextCurrentState>(
	cb: ContextBuilder<T>,
) -> Result<(glutin::Context<NotCurrent>, EventLoop<()>), CreationError> {
	let el = EventLoop::new();
	build_context_headless(cb.clone(), &el).map(|ctx| (ctx, el))
}
