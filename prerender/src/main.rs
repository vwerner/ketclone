//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

mod context;

use assets_fbs::texture_atlas;
use assets_fbs::texture_atlas::generated::{PixelFormat, Rectangle, XY};
use context::Context;
use gfx::gl::{self, types::*};
use gfx::glam::{Mat4, Quat, UVec2, Vec3, Vec4};
use gfx::{
	scene::Camera, scene::Outline, scene::PointLight, scene::VIEW_ANGLE,
	uniform_buffers, AtlasRectangle, MaterialUbo, ModelStore,
	ModelWorldTransform, QuadTransform, RenderQueue, RenderStateStore,
	SectionArg, TexQuadRenderer, WindingOrder,
};
use std::f32::consts::{FRAC_PI_8, PI};
use std::fs::File;
use std::io::Write;
use std::path::Path;

struct Framebuffer {
	fbo: GLuint,
	// Color and depth buffers.
	render_bufs: [GLuint; 2],
}

impl Framebuffer {
	unsafe fn new(dim: usize) -> Framebuffer {
		let mut fbo = 0;
		gl::GenFramebuffers(1, &mut fbo);
		gl::BindFramebuffer(gl::FRAMEBUFFER, fbo);

		// Color and Depth buffer.
		let mut render_bufs = [0, 0];
		gl::GenRenderbuffers(2, &mut render_bufs as *mut _);

		// Allocate and attach the depth buffer...
		gl::BindRenderbuffer(gl::RENDERBUFFER, render_bufs[1]);
		gl::RenderbufferStorage(
			gl::RENDERBUFFER,
			gl::DEPTH24_STENCIL8,
			dim as GLsizei,
			dim as GLsizei,
		);
		gl::FramebufferRenderbuffer(
			gl::DRAW_FRAMEBUFFER,
			gl::DEPTH_STENCIL_ATTACHMENT,
			gl::RENDERBUFFER,
			render_bufs[1],
		);

		// ...then the color buffer.
		gl::BindRenderbuffer(gl::RENDERBUFFER, render_bufs[0]);
		gl::RenderbufferStorage(
			gl::RENDERBUFFER,
			gl::RGBA8,
			dim as GLsizei,
			dim as GLsizei,
		);
		gl::FramebufferRenderbuffer(
			gl::DRAW_FRAMEBUFFER,
			gl::COLOR_ATTACHMENT0,
			gl::RENDERBUFFER,
			render_bufs[0],
		);

		if gl::CheckFramebufferStatus(gl::DRAW_FRAMEBUFFER)
			!= gl::FRAMEBUFFER_COMPLETE
		{
			panic!("framebuffer status isn't GL_FRAMEBUFFER_COMPLETE")
		}

		Framebuffer { fbo, render_bufs }
	}

	// Dimensions must correspond to the one with wich the framebuffer
	// was created.
	// Assumes the framebuffer is bound.
	unsafe fn read_image(&self, dim: usize) -> Vec<GLubyte> {
		// RGBA8: one byte per component.
		let len = 4 * dim * dim;
		let mut buf = vec![0; len];

		gl::ReadBuffer(gl::COLOR_ATTACHMENT0);
		gl::ReadPixels(
			0,
			0,
			dim as GLsizei,
			dim as GLsizei,
			gl::RGBA,
			gl::UNSIGNED_BYTE,
			buf.as_mut_ptr() as *mut GLvoid,
		);

		buf
	}
}

impl Drop for Framebuffer {
	fn drop(&mut self) {
		unsafe {
			gl::DeleteFramebuffers(1, &self.fbo);
			gl::DeleteRenderbuffers(
				2,
				&self.render_bufs as *const _,
			);
		}
	}
}

/// Square framebuffer organized as a packing of rectangles.
///
/// Uses the OpenGL texture system of coordinates: (0,0) is the lower-left.
struct FramebufferAtlas {
	dim: usize,
	rects: Vec<AtlasRectangle>,
	framebuffer: Framebuffer,
}

impl FramebufferAtlas {
	fn new(dim: usize) -> FramebufferAtlas {
		FramebufferAtlas {
			dim,
			rects: Vec::with_capacity(8),
			framebuffer: unsafe { Framebuffer::new(dim) },
		}
	}

	fn push_rect(&mut self, lower_left: UVec2, dims: UVec2) {
		let to_push = AtlasRectangle { lower_left, dims };
		assert!(
			!to_push.overextends(self.dim, self.dim),
			"rect goes out of the atlas"
		);
		assert!(
			!self.rects.iter().any(|r| r.overlaps(to_push)),
			"found overlap",
		);
		dbg!(to_push);
		self.rects.push(to_push);
	}

	unsafe fn set_viewport_for(&self, idx: usize) {
		let rect = self.rects[idx];
		gl::Viewport(
			rect.lower_left.x as GLint,
			rect.lower_left.y as GLint,
			rect.dims.x as GLsizei,
			rect.dims.y as GLsizei,
		);
	}

	fn orthographic_for(&self, idx: usize) -> Mat4 {
		let rect = self.rects[idx];
		let halfw = 0.5 * (rect.dims.x as f32);
		let halfh = 0.5 * (rect.dims.y as f32);
		let halfd = f32::max(halfw, halfh);
		Mat4::orthographic_rh_gl(
			-halfw, halfw, -halfh, halfh, -halfd, halfd,
		)
	}

	#[cfg(build = "debug")]
	fn write_to_file_tiff(&self, path: impl AsRef<Path>) {
		use tiff::encoder::{colortype, TiffEncoder};

		let bytes = unsafe { self.framebuffer.read_image(self.dim) };
		let mut flipped = Vec::with_capacity(bytes.len());
		for v in (0..self.dim).rev() {
			let s = 4 * v * self.dim;
			let o = 4 * self.dim;
			flipped.extend_from_slice(&bytes[s..(s + o)]);
		}

		let flipped = texture_atlas::flip_image_data(
			&bytes,
			PixelFormat::RGBA,
			self.dim,
			self.dim,
		);

		let mut f = File::create(path).expect("failed to create file");
		let mut tiff = TiffEncoder::new(&mut f)
			.expect("failed to write tiff header");
		tiff.write_image::<colortype::RGBA8>(
			self.dim as u32,
			self.dim as u32,
			&flipped,
		)
		.expect("failed to write tiff image");
	}

	fn write_to_file_fbs(&self, path: impl AsRef<Path>) {
		let bytes = unsafe { self.framebuffer.read_image(self.dim) };

		let rects: Vec<_> = self
			.rects
			.iter()
			.map(|r| {
				Rectangle::new(
					&XY::new(
						r.lower_left.x,
						r.lower_left.y,
					),
					&XY::new(r.dims.x, r.dims.y),
				)
			})
			.collect();

		// Assumes RGBA8.
		let (flatbuf, head) = texture_atlas::from::create(
			PixelFormat::RGBA,
			self.dim as u32,
			self.dim as u32,
			Some(&rects),
			&bytes,
		);

		let mut f = File::create(path).expect("failed to create file");
		f.write_all(&flatbuf[head..])
			.expect("failed to write flatbuff into file");
	}
}

fn xy_cameras(radius: f32, center_y: f32, view_angle: f32) -> (Camera, Camera) {
	let (s, c) = view_angle.sin_cos();
	let eye = Vec3::new(0.0, -radius * s, radius * c);
	let center = Vec3::new(0.0, center_y, 0.0);
	let meshes = Camera { eye, center };

	let eye = Vec3::new(0.0, 0.0, radius * c);
	let center = Vec3::ZERO;
	let quads = Camera { eye, center };

	(meshes, quads)
}

fn main() {
	let _ctx = Context::create().unwrap();

	let atlas_dim = 256;
	let mut atlas = FramebufferAtlas::new(atlas_dim);

	// Prepare graphics...
	uniform_buffers! { mod my_ubos {
		pub struct Ubo {
			camera_to_clip_block: Mat4,
			point_light_block: PointLight,
			outline_block: Outline,
		}

		pub struct Materials {
			dummy: [usize; 1],
		}
	}}
	type Ubo = my_ubos::Ubo;
	type Materials = my_ubos::Materials;
	let models = ModelStore::with_model_files(&[
		"./assets/models/witchhat.model",
		"./assets/models/icosphere.model",
	]);
	let render_states = RenderStateStore::new(
		&[
			include_bytes!("../../game/src/fragment_lighting.vs"),
			include_bytes!("../../game/src/silhouette.vs"),
		],
		&[
			include_bytes!("../../game/src/fragment_lighting.fs"),
			include_bytes!("../../game/src/silhouette.fs"),
		],
		&[(0, 0), (1, 1)],
		&[
			"./assets/textures/witchhat.texture",
			"./assets/textures/orrery_purple.texture",
			"./assets/textures/orrery_blue.texture",
			"./assets/textures/orrery_red.texture",
			"./assets/textures/orrery_green.texture",
		],
	);
	let mut ubo =
		Ubo::new(Mat4::IDENTITY, PointLight::default(), Outline(2.0));
	let mut mat_ubo = Materials::new([0]);
	let dummy_mat_key = mat_ubo.key_for([Some(0)]);
	unsafe {
		render_states.block_binding::<Ubo, Mat4>(0);
		render_states.block_binding::<Ubo, PointLight>(0);
		render_states.block_binding::<Ubo, Mat4>(1);
		render_states.block_binding::<Ubo, Outline>(1);
	}
	let mut render_queue = RenderQueue::with_capacity(
		2,
		&render_states,
		&models,
		&mut ubo,
		&mut mat_ubo,
	);

	unsafe {
		gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
	}

	// Hat.
	let base_quat = Quat::from_rotation_z(PI);
	let hat_transform = ModelWorldTransform {
		pos: Vec3::new(0.0, 0.0, 0.0),
		scale: 25.0 * Vec3::ONE,
		quat: base_quat,
	};
	let state_key = render_states.key_for(WindingOrder::CCW, 0, 0);
	render_queue.push_node(hat_transform, 0, state_key, dummy_mat_key);
	let state_key = render_states.key_for(WindingOrder::CW, 1, 0);
	render_queue.push_node(hat_transform, 0, state_key, dummy_mat_key);
	let (camera_3d, camera_quad) = xy_cameras(10.0, 0.0, VIEW_ANGLE);
	let point_light = PointLight {
		pos: camera_3d.look_at() * Vec4::new(40.0, 50.0, 25.0, 1.0),
		color: Vec4::new(1.0, 1.0, 1.0, 1.0),
	};
	*render_queue.ubo().get_mut_of::<PointLight>() = point_light;

	let rect_w = 56;
	let rect_h = 64;

	let cols = atlas_dim / rect_w;
	let n_frame = 7;
	let max_angle = FRAC_PI_8;
	let d_angle = 2.0 * max_angle / (n_frame - 1) as f32;
	for i_frame in 0..n_frame {
		let x = (i_frame % cols) * rect_w;
		let y = (i_frame / cols) * rect_h;
		let angle = max_angle - i_frame as f32 * d_angle;
		let quat = Quat::from_rotation_y(angle);
		render_queue
			.set_transform_with(0, |t| t.quat = base_quat * quat);
		render_queue
			.set_transform_with(1, |t| t.quat = base_quat * quat);
		atlas.push_rect(
			UVec2::new(x as u32, y as u32),
			UVec2::new(rect_w as u32, rect_h as u32),
		);
		unsafe {
			atlas.set_viewport_for(i_frame);
		}
		*render_queue.ubo().get_mut_of::<Mat4>() =
			atlas.orthographic_for(i_frame);

		// Draw.
		render_queue.draw_nodes(&camera_3d.look_at());
	}

	// Orrery.
	let topmost_hat = (1 + n_frame / cols) * rect_h;
	let rect_dims = UVec2::new(32, 32);
	let camera = Camera {
		eye: Vec3::new(0.0, 0.0, 1.0),
		center: Vec3::new(0.0, 0.0, 0.0),
	};
	let point_light = PointLight {
		pos: camera.look_at() * Vec4::new(50.0, 50.0, 25.0, 1.0),
		color: Vec4::new(1.0, 1.0, 1.0, 1.0),
	};
	*render_queue.ubo().get_mut_of::<PointLight>() = point_light;
	let orrery_transform = ModelWorldTransform {
		pos: Vec3::new(0.0, 0.0, 0.0),
		scale: 10.0 * Vec3::ONE,
		quat: Quat::IDENTITY,
	};
	*render_queue.ubo().get_mut_of::<Outline>() = Outline(1.5);
	for orrery_idx in 0..4 {
		let lower_left = UVec2::new(
			orrery_idx * rect_dims.x,
			topmost_hat as u32,
		);
		atlas.push_rect(lower_left, rect_dims);
		let rect_idx = n_frame + orrery_idx as usize;
		unsafe {
			atlas.set_viewport_for(rect_idx);
		}
		*render_queue.ubo().get_mut_of::<Mat4>() =
			atlas.orthographic_for(rect_idx);
		render_queue.clear();
		let tex_idx = (orrery_idx + 1) as usize;
		render_queue.push_node(
			orrery_transform,
			1,
			render_states.key_for(WindingOrder::CCW, 0, tex_idx),
			dummy_mat_key,
		);
		render_queue.push_node(
			orrery_transform,
			1,
			render_states.key_for(WindingOrder::CW, 1, tex_idx),
			dummy_mat_key,
		);
		render_queue.draw_nodes(&camera.look_at());
	}

	// Normal shot.
	let shot_rect_idx = 11;
	let atlases = [
		"assets/wip/shootingstar.texture",
		"assets/wip/laser.texture",
		"assets/wip/hitbox.texture",
	];
	let sections = [
		SectionArg {
			capacity: 1,
			z: 0.0,
			texture_idx: 0,
			overlay_color: Vec4::ONE,
		},
		SectionArg {
			capacity: 1,
			z: 0.0,
			texture_idx: 1,
			overlay_color: Vec4::ONE,
		},
		SectionArg {
			capacity: 1,
			z: 0.0,
			texture_idx: 2,
			overlay_color: Vec4::ONE,
		},
	];
	let mut quad_renderer = TexQuadRenderer::new(&atlases, &sections);
	let lower_left = UVec2::new(170, 64);
	let texture_rect = quad_renderer.get_rect_for(0, 0);
	atlas.push_rect(lower_left, texture_rect.dims);
	let image_to_clip =
		atlas.orthographic_for(shot_rect_idx) * camera_quad.look_at();
	let shot_transform = QuadTransform::IDENTITY;
	unsafe {
		quad_renderer.push_to_section(0, shot_transform, 0);
		atlas.set_viewport_for(shot_rect_idx);
		quad_renderer.draw(image_to_clip);
	}
	quad_renderer.clear();

	// Laser.
	let laser_rect_idx = 12;
	let lower_left = UVec2::new(243, 0);
	let texture_rect = quad_renderer.get_rect_for(1, 0);
	atlas.push_rect(lower_left, texture_rect.dims);
	let transform = QuadTransform::IDENTITY;
	let image_to_clip =
		atlas.orthographic_for(laser_rect_idx) * camera_quad.look_at();
	unsafe {
		quad_renderer.push_to_section(1, transform, 0);
		atlas.set_viewport_for(laser_rect_idx);
		quad_renderer.draw(image_to_clip);
	}
	quad_renderer.clear();

	// Hitbox.
	let hitbox_rect_idx = 13;
	let lower_left = UVec2::new(10, 200);
	let texture_rect = quad_renderer.get_rect_for(2, 0);
	atlas.push_rect(lower_left, texture_rect.dims);
	let transform = QuadTransform::IDENTITY;
	// Doesn't work, for some reason.
	// TODO: Figure out why.
	// let image_to_clip = atlas.orthographic_for(hitbox_rect_idx) * camera_quad.look_at();
	let image_to_clip = Mat4::IDENTITY;
	unsafe {
		quad_renderer.push_to_section(2, transform, 0);
		atlas.set_viewport_for(hitbox_rect_idx);
		quad_renderer.draw(image_to_clip);
	}
	quad_renderer.clear();

	// Save to image.
	atlas.write_to_file_fbs("prerender_test.fbs");
	#[cfg(build = "debug")]
	atlas.write_to_file_tiff("prerender_test.tiff");

	gfx::glerr!();
}
