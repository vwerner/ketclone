//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

use assets_fbs::texture_atlas::from::tiff;
use std::fs::File;
use std::io::{Read, Write};
use std::path::Path;

/// Convert a blender-generated wavefront .obj into a model file.
fn wavefront_to_flatbuf<P>(obj_path: P, out_path: P)
where
	P: AsRef<Path>,
{
	let obj = {
		let mut file =
			File::open(obj_path).expect("could not open file");
		let mut buf = String::new();
		file.read_to_string(&mut buf).expect("failed to read file");
		buf
	};
	let (header_bytes, flatbuf) =
		assets_fbs::model::from::from_wavefront(&obj);

	let mut out_file =
		File::create(out_path).expect("could no create output file");
	out_file.write_all(&header_bytes)
		.expect("could not write the header to the output file");
	out_file.write_all(&flatbuf)
		.expect("could not write the data to the output file");
}

/// Write a texture flatbuffer from a tif file.
fn tiff_to_atlas<P>(tiff_path: P, out_path: P)
where
	P: AsRef<Path>,
{
	let mut tiff = tiff::decoder::Decoder::new(
		File::open(tiff_path).expect("could not open tiff file"),
	)
	.expect("could not decode file as tiff");
	let (flatbuf, head) =
		assets_fbs::texture_atlas::from::from_tiff(&mut tiff);

	let mut out_file =
		File::create(out_path).expect("could not create output file");
	out_file.write_all(&flatbuf[head..])
		.expect("could not write the flatbuf to the output file");
}

fn main() {
	let in_path = std::env::args().nth(1).expect("no path given");
	let in_path = Path::new(&in_path);
	let out_path = &std::env::args().nth(2).expect("no output path given");
	let out_path = Path::new(&out_path);

	let in_ext = in_path
		.extension()
		.expect("file have no extension")
		.to_string_lossy();
	match in_ext.as_ref() {
		"obj" => wavefront_to_flatbuf(&in_path, &out_path),
		"tif" => tiff_to_atlas(&in_path, &out_path),
		_ => panic!("could not recognize filetype from extension"),
	};
}
