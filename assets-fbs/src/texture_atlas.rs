//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

#[allow(unused_imports)]
#[allow(non_snake_case)]
#[allow(clippy::all)]
#[path = "../target/flatbuffers/texture_atlas_generated.rs"]
pub mod generated;

use generated::PixelFormat;

/// Flips an image upside down.
///
/// Returns an upside-down copy of the given image bytes.

/// This is a convenience function to translate between OpenGL's texture
/// coordinates (origin at lower-left) and traditional image formats'
/// coordinates (origin at top-left).
pub fn flip_image_data(
	bytes: &[u8],
	format: PixelFormat,
	width: usize,
	height: usize,
) -> Vec<u8> {
	let bytes_per_pixel = match format {
		PixelFormat::RGB => 3,
		PixelFormat::RGBA => 4,
		_ => unreachable!("bad pixel format"),
	};

	let mut flipped = Vec::with_capacity(bytes.len());

	for v in (0..height).rev() {
		let s = bytes_per_pixel * v * width;
		let o = bytes_per_pixel * width;
		flipped.extend_from_slice(&bytes[s..(s + o)]);
	}

	flipped
}

#[cfg(feature = "from")]
pub mod from {
	use super::generated::{
		PixelFormat, Rectangle, TextureAtlas, TextureAtlasArgs, XY,
	};
	use std::borrow::Borrow;
	use std::io::{Read, Seek};
	pub use tiff;
	use tiff::{
		decoder::{Decoder, DecodingResult},
		tags::Tag,
	};

	/// Create a flatbuffer from a tiff image.
	///
	/// The "atlas" will be considered as just the image in its whole,
	/// and will have a single rectangle covering its entirety.
	pub fn from_tiff<R: Read + Seek>(
		tiff: &mut Decoder<R>,
	) -> (Vec<u8>, usize) {
		let width = tiff
			.get_tag_u32(Tag::ImageWidth)
			.expect("could not get ImageWidth tag");
		let height = tiff
			.get_tag_u32(Tag::ImageLength)
			.expect("could not get ImageHeight tag");
		let image_data = match tiff.read_image() {
			Ok(DecodingResult::U8(vec)) => vec,
			Err(_) => panic!("error while decoding image"),
			_ => unimplemented!(
				"unsupported image internal format"
			),
		};
		let colortype =
			tiff.colortype().expect("error reading tiff colortype");
		let format = match colortype {
			tiff::ColorType::RGB(_) => PixelFormat::RGB,
			tiff::ColorType::RGBA(_) => PixelFormat::RGBA,
			_ => unimplemented!("unsupported tiff colortype"),
		};
		let rect = [Rectangle::new(
			&XY::new(0, 0),
			&XY::new(width, height),
		)];
		let rects = Some(rect.borrow());

		let flipped = super::flip_image_data(
			&image_data,
			format,
			width as usize,
			height as usize,
		);

		create(format, width, height, rects, &flipped)
	}

	/// Create an atlas flatbuffer.
	///
	/// Returns the owned buffer and the start index of valid data.
	pub fn create(
		format: PixelFormat,
		width: u32,
		height: u32,
		rects: Option<&[Rectangle]>,
		image_data: &[u8],
	) -> (Vec<u8>, usize) {
		let cap = 2 * image_data.len();
		let mut builder =
			flatbuffers::FlatBufferBuilder::with_capacity(cap);
		let rects = rects.map(|r| builder.create_vector(r));
		let image_data = Some(builder.create_vector(image_data));
		let atlas = TextureAtlas::create(
			&mut builder,
			&TextureAtlasArgs {
				format,
				width,
				height,
				rects,
				image_data,
			},
		);

		builder.finish_minimal(atlas);
		builder.collapse()
	}
}
