//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

#[allow(unused_imports)]
#[allow(non_snake_case)]
#[allow(clippy::all)]
#[path = "../target/flatbuffers/model_generated.rs"]
pub mod generated;

pub mod header {
	use core::mem;
	use flatbuffers::EndianScalar;

	/// An header with the vertices and indices count.
	///
	/// This header allows one to only peek at the first bytes of the
	/// file to know how much storage is needed for the model. This way,
	/// if you want to load multiple models into one buffer, you can do
	/// a first pass to fetch each header, compute the total space needed
	/// and allocate the GPU buffer. Then, the actual data can be uploaded
	/// one file after another.
	// Be coherent with the flatbuffers and enforce little endian
	// byte order.
	// Must be portable! Enforce padding, etc. Is repr(C) strong enough
	// of a guarentee?
	#[repr(C)]
	#[derive(Clone, Copy)]
	pub struct Header {
		// Must be the same as the index type.
		pub vertices_count: u16,
		// Must be the same as the `indices` argument of the DrawCommand.
		pub indices_count: i32,
	}

	impl Header {
		pub const BYTE_SIZE: usize = mem::size_of::<Header>();

		pub fn from_bytes_exact(
			bytes: [u8; Header::BYTE_SIZE],
		) -> Header {
			let mut header: Header =
				unsafe { mem::transmute(bytes) };
			header.vertices_count =
				header.vertices_count.from_little_endian();
			header.indices_count =
				header.indices_count.from_little_endian();
			header
		}

		pub fn as_le_bytes(mut self) -> [u8; Header::BYTE_SIZE] {
			self.vertices_count = self.vertices_count.to_le();
			self.indices_count = self.indices_count.to_le();
			unsafe { mem::transmute(self) }
		}
	}
}

pub mod read {
	use crate::model::header::Header;
	use std::fs::File;
	use std::io::Read;
	use std::path::Path;

	#[derive(Debug)]
	pub enum Error {
		BufferNotLargeEnough,
		FileOpenFailed(std::io::Error),
		ReadFailed(std::io::Error),
		DidntReadAll,
	}

	/// Open a model file and read only the header into a buffer.
	pub fn read_header<P>(path: P, buf: &mut [u8]) -> Result<(), Error>
	where
		P: AsRef<Path>,
	{
		if buf.len() < Header::BYTE_SIZE {
			return Err(Error::BufferNotLargeEnough);
		}
		let file = File::open(path).map_err(Error::FileOpenFailed)?;
		file.take(Header::BYTE_SIZE as u64)
			.read(buf)
			.map_err(Error::ReadFailed)
			.and_then(|b| {
				if b != Header::BYTE_SIZE {
					Err(Error::DidntReadAll)
				} else {
					Ok(())
				}
			})
	}

	// Read the complete file, header and flatbuffer.
	//
	// Skip the header portion by indexing with `[Header::BYTE_SIZE..]`.
	pub fn read_all_to_end<P>(
		path: P,
		buf: &mut Vec<u8>,
	) -> Result<usize, Error>
	where
		P: AsRef<Path>,
	{
		let mut file =
			File::open(path).map_err(Error::FileOpenFailed)?;
		file.read_to_end(buf).map_err(Error::ReadFailed)
	}
}

#[cfg(feature = "from")]
pub mod from {
	use crate::model::generated::{
		DrawCommand, Model, ModelArgs, Primitive, VertexNormal,
		VertexPos, VertexUv,
	};
	use crate::model::header::Header;
	use std::collections::hash_map::{Entry, HashMap};
	use std::mem;
	use wavefront_obj::obj;

	impl From<obj::Vertex> for VertexPos {
		fn from(v: obj::Vertex) -> VertexPos {
			VertexPos::new(v.x as f32, v.y as f32, v.z as f32)
		}
	}

	impl From<obj::Vertex> for VertexNormal {
		fn from(vn: obj::Vertex) -> VertexNormal {
			VertexNormal::new(vn.x as f32, vn.y as f32, vn.z as f32)
		}
	}

	impl From<obj::TVertex> for VertexUv {
		fn from(vt: obj::TVertex) -> VertexUv {
			VertexUv::new(vt.u as f32, 1.0 - vt.v as f32)
		}
	}

	/// A VTNIndex that assumes the texture and normal indices are present.
	#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
	struct Vtn(usize, usize, usize);

	impl From<obj::VTNIndex> for Vtn {
		fn from(vtn: obj::VTNIndex) -> Vtn {
			Vtn(
				vtn.0,
				vtn.1.expect("missing texture index"),
				vtn.2.expect("missing normal index"),
			)
		}
	}

	/// The VTN indices of a obj triangle primitive.
	#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
	struct TriangleVtn(Vtn, Vtn, Vtn);

	impl From<obj::Primitive> for TriangleVtn {
		fn from(primitive: obj::Primitive) -> TriangleVtn {
			if let obj::Primitive::Triangle(v0, v1, v2) = primitive
			{
				TriangleVtn(
					From::from(v0),
					From::from(v1),
					From::from(v2),
				)
			} else {
				unimplemented!(
				"non-triangle primitive not implemented"
			);
			}
		}
	}

	/// Parses a wavefront object into a model.
	///
	/// Only parses the first geometry. Returns the bytes of the model
	/// header and the bytes of the flatbuffer.
	// TODO: Triangle fans and strip. Since objs actually only describe
	// single triangles, this will probably need looking at the whole
	// topology after parsing all shapes.
	// TODO: Objects with multiple Geometries.
	// TODO: Groups and smoothing groups. What are those exactly?
	// NOTE: obj indices start from 1.
	pub fn from_wavefront(obj: &str) -> ([u8; Header::BYTE_SIZE], Vec<u8>) {
		// In a wavefront, all attributes are defined in independent
		// sets. Vertices are defined by picking individually from
		// each of theses sets (3 indices per vertex). In contrast,
		// my format expect a set of tuples of attributes, and
		// defines vertices by picking one of these tuple (1 index).

		let objset = obj::parse(&obj).expect("failed to parse obj");
		let obj = objset
			.objects
			.get(0)
			.expect("object set has no objects");

		let cap = obj.geometry[0].shapes.len() * 3;
		let mut pos: Vec<VertexPos> = Vec::with_capacity(cap);
		let mut normals: Vec<VertexNormal> = Vec::with_capacity(cap);
		let mut uvs: Vec<VertexUv> = Vec::with_capacity(cap);
		let mut indices: Vec<u16> = Vec::with_capacity(cap);
		// Map from unique tuples of (pos, normal, uv) to index.
		let mut vtn_to_idx = HashMap::with_capacity(cap);
		let it = obj.geometry[0]
			.shapes
			.iter()
			.map(|s| TriangleVtn::from(s.primitive));
		for trivtn in it {
			// Push a new index if the vtn wasn't encoutered
			// before, and push the new vertex data. If it
			// was encountered already, get and push the
			// corresponding index.
			let mut push_vtn = |vtn: Vtn| match vtn_to_idx
				.entry(vtn)
			{
				Entry::Vacant(v) => {
					indices.push(
						*v.insert(pos.len() as u16)
					);
					pos.push(From::from(
						obj.vertices[vtn.0],
					));
					normals.push(From::from(
						obj.normals[vtn.2],
					));
					uvs.push(From::from(
						obj.tex_vertices[vtn.1],
					));
				}
				Entry::Occupied(o) => {
					indices.push(Clone::clone(o.get()));
				}
			};
			push_vtn(trivtn.0);
			push_vtn(trivtn.1);
			push_vtn(trivtn.2);
		}

		// Draw commands. Since we only parse the first geometry
		// for now, there's only one draw command.
		let cmds = [DrawCommand::new(
			Primitive::Triangle,
			(obj.geometry[0].shapes.len() * 3) as i32,
			0i32, // will have to change when parsing multipl geometries.
			0i32, // basevertex is always 0 in model files. Then why have it be a field?
		)];

		// Debug.
		assert_eq!(pos.len(), normals.len());
		assert_eq!(pos.len(), uvs.len());
		dbg!(pos.len(), indices.len());

		// Create the header.
		let header_bytes = Header {
			vertices_count: pos.len() as u16,
			indices_count: indices.len() as i32,
		}
		.as_le_bytes();

		// Create the flatbuffer.
		fn slice_size_of<T>(s: &[T]) -> usize {
			s.len() * mem::size_of::<T>()
		}
		let cap = (slice_size_of(&pos)
			+ slice_size_of(&normals)
			+ slice_size_of(&uvs) + slice_size_of(&cmds))
			* 2;
		let mut builder =
			flatbuffers::FlatBufferBuilder::with_capacity(cap);
		let pos = Some(builder.create_vector(&pos));
		let normals = Some(builder.create_vector(&normals));
		let uvs = Some(builder.create_vector(&uvs));
		let indices = Some(builder.create_vector(&indices));
		let cmds = Some(builder.create_vector(&cmds));
		let model = Model::create(
			&mut builder,
			&ModelArgs {
				pos,
				normals,
				uvs,
				indices,
				cmds,
			},
		);
		builder.finish_minimal(model);
		let flatbuf = builder.finished_data();

		// TODO: There's a copy of the whole flatbuffer here.
		(header_bytes, flatbuf.to_vec())
	}
}
