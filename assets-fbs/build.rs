//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

extern crate flatc_rust;

use std::path::Path;

fn main() {
	for fb in ["model", "texture_atlas"].iter() {
		println!("cargo:rerun-if-changed=src/{}.fbs", fb);
		flatc_rust::run(flatc_rust::Args {
			inputs: &[Path::new(&format!("src/{}.fbs", fb))],
			out_dir: Path::new("target/flatbuffers/"),
			..Default::default()
		})
		.unwrap_or_else(|_| {
			panic!("failed to generate the {} flat buffer code", fb)
		});
	}
}
