//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

use crate::kinetics::{AsScalarSlice, AsScalarSliceMut, Vec2};
use crate::{WINDOW_HEIGHT_F, WINDOW_WIDTH_F};
use gfx::{
	MaterialUbo, MaterialUboKey, ModelWorldTransform, QuadTransform,
	RenderQueue, RenderStateKey, TexQuadRenderer, Ubo,
};
use itertools::{iproduct, izip};
use std::ops::Range;

/// Rectangle hitbox.
///
/// Constrained to integer dimensions.
#[derive(Clone, Copy, Debug)]
pub struct Hitbox {
	pub dims: Vec2,
}

impl Hitbox {
	pub const NONE: Hitbox = Hitbox { dims: Vec2::ZERO };

	pub fn new(width: u32, height: u32) -> Hitbox {
		Hitbox {
			dims: Vec2::new(width as f32, height as f32),
		}
	}

	fn test_hit(
		pos1: Vec2,
		hit1: Hitbox,
		pos2: Vec2,
		hit2: Hitbox,
	) -> bool {
		let hdim1 = 0.5 * hit1.dims;
		let hdim2 = 0.5 * hit2.dims;
		let x_overlap = (pos1.x + hdim1.x) > (pos2.x - hdim2.x)
			&& (pos1.x - hdim1.x) < (pos2.x + hdim2.x);
		let y_overlap = (pos1.y + hdim1.y) > (pos2.y - hdim2.y)
			&& (pos1.y - hdim1.y) < (pos2.y + hdim2.y);
		x_overlap && y_overlap
	}
}

/// Graphical data needed to draw an object.
#[derive(Copy, Clone, Debug)]
pub enum GfxData {
	// Index into a render queue, with no guarentee on lifetime and stuff.
	Model(usize),
	// Section and atlas rectangle index.
	Quad(usize, usize),
	None,
}

impl GfxData {
	// TODO: Return the index of the pushed model.
	pub fn model<'s, U: Ubo, M: MaterialUbo>(
		transform: ModelWorldTransform,
		model_idx: usize,
		state_key: RenderStateKey<'s>,
		mat_key: MaterialUboKey,
		queue: &mut RenderQueue<'s, '_, '_, '_, U, M>,
	) -> GfxData {
		// This is only safe assuming the only way to get a key
		// from the queue already checks the validity of a key. And
		// that the same queue without changes is used to render...
		let idx = unsafe {
			queue.push_node_key(
				transform, model_idx, state_key, mat_key,
			)
		};

		GfxData::Model(idx)
	}

	fn quad(secidx: usize, rectidx: usize) -> GfxData {
		GfxData::Quad(secidx, rectidx)
	}
}

pub trait BoardSection {
	const RANGE: Range<usize>;
	const LEN: usize;
}

macro_rules! board_sections {
	($($name:ident : $range:expr),+ $(,)?) => {
		$( pub enum $name {}
		impl BoardSection for $name {
			const RANGE: std::ops::Range<usize> = $range;
			const LEN: usize = $range.end - $range.start;
		} )+
	};
}

board_sections! {
	Player: 0..1,
	Orrery: 1..5,
	Shots: 5..128,
	Enemies: 128..132,
	Bullets: 132..Board::MAX_OBJECTS,
	All: 0..Board::MAX_OBJECTS,
}

/// Array of constant size for board objects.
type BoardArray<T> = [T; Board::MAX_OBJECTS];

/// List of game objects on game board.
pub struct Board {
	positions: BoardArray<Vec2>,
	velocities: BoardArray<Vec2>,
	alive: BoardArray<bool>,
	hitboxes: BoardArray<Hitbox>,
	gfx: BoardArray<GfxData>,
}

impl Board {
	/// Maximum count of objects on the board.
	pub const MAX_OBJECTS: usize = 512;

	/// Create an empty board.
	pub fn new() -> Board {
		Board {
			positions: [Vec2::ZERO; Board::MAX_OBJECTS],
			velocities: [Vec2::ZERO; Board::MAX_OBJECTS],
			alive: [false; Board::MAX_OBJECTS],
			hitboxes: [Hitbox::NONE; Self::MAX_OBJECTS],
			gfx: [GfxData::None; Board::MAX_OBJECTS],
		}
	}

	/// Push a new object in the list.
	///
	/// Returns the index of the new object.
	///
	/// # Panics
	///
	/// Panic if no free slot if available.
	pub fn push<S: BoardSection>(
		&mut self,
		pos: Vec2,
		vel: Vec2,
		gfx: GfxData,
		hitbox: Hitbox,
	) -> usize {
		// TODO: Might be sped up by replacing alive with vector
		// masks. Maybe rustc works that out itself? Check.

		// Safe because S::RANGE is within 0..Board::MAX_OBJECTS.
		let mut it = unsafe {
			izip!(
				S::RANGE,
				self.alive
					.get_unchecked_mut(S::RANGE)
					.iter_mut(),
				self.positions
					.get_unchecked_mut(S::RANGE)
					.iter_mut(),
				self.velocities
					.get_unchecked_mut(S::RANGE)
					.iter_mut(),
				self.gfx.get_unchecked_mut(S::RANGE).iter_mut(),
				self.hitboxes
					.get_unchecked_mut(S::RANGE)
					.iter_mut(),
			)
		};
		let (idx, blive, bpos, bvel, bgfx, bhitbox) = it
			.find(|(_, live, _, _, _, _)| !**live)
			.expect("no free slot in the section");

		*blive = true;
		*bpos = pos;
		*bvel = vel;
		*bgfx = gfx;
		*bhitbox = hitbox;

		idx
	}

	/// Move each object for the current frame.
	pub fn kinetics(&mut self) {
		// Safe because 0..Board::MAX_OBJECTS is the full range. Very
		// redundant though...
		let it = unsafe {
			izip!(
				self.positions
					.get_unchecked_mut(
						0..Board::MAX_OBJECTS
					)
					.as_scalar_slice_mut()
					.iter_mut(),
				self.velocities
					.get_unchecked(0..Board::MAX_OBJECTS)
					.as_scalar_slice()
					.iter(),
			)
		};
		// Could filter dead objects, but I'll assume computing
		// useless kinetics is worth it to avoid branching.
		// TODO: Back up that assumption.
		// TODO: Check that rustc auto-vectorizes this.
		for (pos, vel) in it {
			*pos += *vel;
		}
	}

	/// Mark objects too far out the screen as dead.
	pub fn kill_out(&mut self) {
		// +/- ranges.
		const X_LIMIT: f32 = WINDOW_WIDTH_F;
		const Y_LIMIT: f32 = WINDOW_HEIGHT_F;

		let it = izip!(
			self.alive.iter_mut(),
			self.positions.iter(),
			self.velocities.iter_mut()
		)
		.filter(|(_, pos, _)| {
			pos.x.abs() > X_LIMIT || pos.y.abs() > Y_LIMIT
		});

		// Set velocity to zero so that doing dead objects' kinetics
		// is a no-op. Else, it'd risk having positions going to inf.
		for (flag, _, vel) in it {
			*flag = false;
			*vel = Vec2::ZERO;
		}
	}

	/// Send graphical data to renderers to draw the board.
	pub fn send_gfx_data<U: Ubo, M: MaterialUbo>(
		&self,
		model_queue: &mut RenderQueue<U, M>,
		quad_renderer: &mut TexQuadRenderer,
	) {
		let it = izip!(
			self.alive.iter(),
			self.positions.iter(),
			self.gfx.iter()
		)
		.filter(|(live, _, _)| **live);

		for (_, pos, gfx) in it {
			Board::send_object_gfx_data(
				*pos,
				*gfx,
				model_queue,
				quad_renderer,
			);
		}
	}

	fn send_object_gfx_data<U: Ubo, M: MaterialUbo>(
		pos: Vec2,
		gfx_data: GfxData,
		_model_queue: &mut RenderQueue<U, M>,
		quad_renderer: &mut TexQuadRenderer,
	) {
		match gfx_data {
			GfxData::Quad(secidx, rectidx) => {
				let transform = QuadTransform {
					// NOT rounded!
					pos,
					..QuadTransform::IDENTITY
				};
				// TODO: Why is this unsafe again?
				unsafe {
					quad_renderer.push_to_section(
						secidx, transform, rectidx,
					);
				}
			}
			GfxData::Model(_) => (),
			GfxData::None => (),
		}
	}

	/// Create textured quads to visualize hitboxes.
	pub fn visualize_hitboxes(&self, renderer: &mut TexQuadRenderer) {
		let it = izip!(
			self.alive.iter(),
			self.positions.iter(),
			self.hitboxes.iter()
		)
		.filter(|(live, _, _)| **live);

		for (_, pos, hit) in it {
			let transform = QuadTransform {
				// NOT rounded!
				pos: *pos,
				scale: hit.dims,
				..QuadTransform::IDENTITY
			};
			// Assume 13 is a fitting rect index.
			unsafe { renderer.push_to_section(0, transform, 13) }
		}
	}

	pub fn test_hits<F>(&mut self, mut f: F)
	where
		F: FnMut(GfxData),
	{
		// All iterators created below are safe because S::RANGE
		// is within 0..Board::MAX_OBJECTS.

		let it = iproduct!(Shots::RANGE, Enemies::RANGE);
		for (idx_shot, idx_enemy) in it {
			if !(self.alive[idx_shot] && self.alive[idx_enemy]) {
				continue;
			}
			if Hitbox::test_hit(
				self.positions[idx_shot],
				self.hitboxes[idx_shot],
				self.positions[idx_enemy],
				self.hitboxes[idx_enemy],
			) {
				f(self.gfx[idx_enemy]);
				self.alive[idx_shot] = false;
			}
		}

		// Player vs. enemy bodies.
		let player_it = unsafe {
			izip!(
				self.alive.get_unchecked(Player::RANGE).iter(),
				self.positions
					.get_unchecked(Player::RANGE)
					.iter(),
				self.hitboxes
					.get_unchecked(Player::RANGE)
					.iter(),
			)
			.filter_map(|(live, pos, hit)| live.then(|| (pos, hit)))
		};
		let enemy_it = unsafe {
			izip!(
				self.alive.get_unchecked(Enemies::RANGE).iter(),
				self.positions
					.get_unchecked(Enemies::RANGE)
					.iter(),
				self.hitboxes
					.get_unchecked(Enemies::RANGE)
					.iter(),
			)
			.filter_map(|(live, pos, hit)| live.then(|| (pos, hit)))
		};
		let it = iproduct!(player_it, enemy_it);
		for ((player_pos, player_hit), (enemy_pos, enemy_hit)) in it {
			if Hitbox::test_hit(
				*player_pos,
				*player_hit,
				*enemy_pos,
				*enemy_hit,
			) {
				println!("Player-enemy collision");
			}
		}
	}

	pub fn positions<S: BoardSection>(&mut self) -> &[Vec2] {
		// Safe because S::RANGE is within 0..Board::MAX_OBJECTS.
		unsafe { self.positions.get_unchecked(S::RANGE) }
	}

	pub fn alive<S: BoardSection>(&mut self) -> &[bool] {
		// Safe because S::RANGE is within 0..Board::MAX_OBJECTS.
		unsafe { self.alive.get_unchecked(S::RANGE) }
	}

	pub fn alive_mut<S: BoardSection>(&mut self) -> &mut [bool] {
		// Safe because S::RANGE is within 0..Board::MAX_OBJECTS.
		unsafe { self.alive.get_unchecked_mut(S::RANGE) }
	}

	pub fn velocities<S: BoardSection>(&mut self) -> &[Vec2] {
		// Safe because S::RANGE is within 0..Board::MAX_OBJECTS.
		unsafe { self.velocities.get_unchecked(S::RANGE) }
	}

	pub fn velocities_mut<S: BoardSection>(&mut self) -> &mut [Vec2] {
		// Safe because S::RANGE is within 0..Board::MAX_OBJECTS.
		unsafe { self.velocities.get_unchecked_mut(S::RANGE) }
	}

	pub fn hitboxes<S: BoardSection>(&self) -> &[Hitbox] {
		// Safe because S::RANGE is within 0..Board::MAX_OBJECTS.
		unsafe { self.hitboxes.get_unchecked(S::RANGE) }
	}

	pub fn hitboxes_mut<S: BoardSection>(&mut self) -> &mut [Hitbox] {
		// Safe because S::RANGE is within 0..Board::MAX_OBJECTS.
		unsafe { self.hitboxes.get_unchecked_mut(S::RANGE) }
	}

	pub fn gfx_data_mut<S: BoardSection>(&mut self) -> &mut [GfxData] {
		// Safe because S::RANGE is within 0..Board::MAX_OBJECTS.
		unsafe { self.gfx.get_unchecked_mut(S::RANGE) }
	}
}
