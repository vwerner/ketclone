//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

// #![allow(unused)]

use crate::controller::Controller;
use gfx::glam;
use std::slice;

const LASER_VEL: f32 = 31.0;

pub type Vec2 = glam::Vec2;

pub trait AsScalarSlice<'a> {
	type Scalar;
	// Implemented on &[T], hence the `as_` prefix.
	#[allow(clippy::wrong_self_convention)]
	fn as_scalar_slice(self) -> &'a [Self::Scalar];
}

pub trait AsScalarSliceMut<'a> {
	type Scalar;
	#[allow(clippy::wrong_self_convention)]
	fn as_scalar_slice_mut(self) -> &'a mut [Self::Scalar];
}

impl<'a> AsScalarSlice<'a> for &'a [Vec2] {
	type Scalar = f32;
	fn as_scalar_slice(self) -> &'a [f32] {
		// Safe because the layout of Vec2 is the same as [f32; 2].
		unsafe {
			slice::from_raw_parts::<'a>(
				self as *const _ as *const f32,
				2 * self.len(),
			)
		}
	}
}

impl<'a> AsScalarSliceMut<'a> for &'a mut [Vec2] {
	type Scalar = f32;
	fn as_scalar_slice_mut(self) -> &'a mut [f32] {
		// Safe because the layout of Vec2 is the same as [f32; 2].
		unsafe {
			slice::from_raw_parts_mut::<'a>(
				self as *mut _ as *mut f32,
				2 * self.len(),
			)
		}
	}
}

pub fn angle_between(left: Vec2, right: Vec2) -> f32 {
	// TODO: is that name really appropriate? Is that computation
	// really the best I can do?
	use std::f32::consts::FRAC_PI_2;
	let dx = left.x - right.x;
	let dy = left.y - right.y;
	f32::atan2(dy, dx) + FRAC_PI_2
}

/// Returns the player character velocity from the [`Controller`] bits.
pub fn player_ctrl_vel(ctrl: Controller) -> Vec2 {
	use std::f32::consts::FRAC_1_SQRT_2;

	const VEL: f32 = 4.8;
	const DIAG_VEL: f32 = FRAC_1_SQRT_2 * VEL;

	match ctrl.0 & Controller::DIRECTIONS {
		0 => Vec2::new(0.0, 0.0),
		Controller::RIGHT => Vec2::new(VEL, 0.0),
		Controller::UP => Vec2::new(0.0, VEL),
		Controller::LEFT => Vec2::new(-VEL, 0.0),
		Controller::DOWN => Vec2::new(0.0, -VEL),
		Controller::RIGHT_UP => Vec2::new(DIAG_VEL, DIAG_VEL),
		Controller::RIGHT_DOWN => Vec2::new(DIAG_VEL, -DIAG_VEL),
		Controller::UP_LEFT => Vec2::new(-DIAG_VEL, DIAG_VEL),
		Controller::LEFT_DOWN => Vec2::new(-DIAG_VEL, -DIAG_VEL),
		_ => unreachable!("illegal direction inputs"),
	}
}

/// Compute the velocity of the laser heads.
///
/// Depends on the current position of the heads, the position/velocity of
/// the lasers' origins, and the position/velocity of the target.
pub fn laser_head_vel(
	_o_pos: &[Vec2],
	_o_vel: &[Vec2],
	_t_pos: &[Vec2],
	_t_vel: Vec2,
	_h_pos: &[Vec2],
	_h_vel: &mut [Vec2],
) {
	// Obtained by deriving the kinetic equation
	// `h(t) = o(t) + V × t × n(t)`
	// w.r.t. time `t`, with `h` the laser's head position, `o` its
	// origin, `V` a constant growth velocity, and `n` the normal toward
	// its target.

	// Due to numerical errors, the laser head can fly past the target
	// or creep away when it's already on it. To prevent that, the head is
	// snapped to the target exactly if the distance from it is less than V.

	// Vectorized version...
	todo!()
}

/// Compute the velocity of a laser head.
///
/// Depends on the current position of the head, the position/velocity of
/// the laser's origin, and the position/velocity of the target.
pub fn laser_head_vel_scalar(
	h_pos: Vec2,
	o_pos: Vec2,
	o_vel: Vec2,
	t_pos: Vec2,
	t_vel: Vec2,
) -> Vec2 {
	// Obtained by deriving the kinetic equation
	// `h(t) = o(t) + V × t × n(t)`
	// w.r.t. time `t`, with `h` the laser's head position, `o` its
	// origin, `V` a constant growth velocity, and `n` the normal toward
	// its target.

	// Due to numerical errors, the laser head can fly past the target
	// or creep away when it's already on it. To prevent that, the head is
	// snapped to the target exactly if the distance from it is less than V.

	// let h_pos = DfixVec::from_fixvec(h_pos);
	// let o_pos = DfixVec::from_fixvec(o_pos);
	// let o_vel = DfixVec::from_fixvec(o_vel);
	// let t_pos = DfixVec::from_fixvec(t_pos);
	// let t_vel = DfixVec::from_fixvec(t_vel);
	// let laser_vel = DfixScalar::from_num(LASER_VEL);
	let laser_vel2 = LASER_VEL * LASER_VEL;

	let r_oh = h_pos - o_pos;
	let d_oh = r_oh.length();

	let r_ht = t_pos - h_pos;
	let d2_ht = r_ht.length_squared();
	if d2_ht <= laser_vel2 {
		return r_ht;
	}
	let d_ht = d2_ht.sqrt();
	let laser_vel = f32::min(LASER_VEL, d_ht);

	let r_ot = t_pos - o_pos;
	let d2_ot = r_ot.length_squared();
	if d2_ot <= laser_vel2 && d_oh < 1.0E-6 {
		return r_ht;
	}
	let d_ot = d2_ot.sqrt();

	let normal = r_ot / d_ot;

	let v_ot = t_vel - o_vel;
	let scal = Vec2::dot(v_ot, r_ot);
	o_vel + normal * laser_vel + v_ot * (d_oh / d_ot)
		- (normal * (d_oh * scal)) / d2_ot
}
