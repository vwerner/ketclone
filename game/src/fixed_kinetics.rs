//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

use crate::controller::Controller;
use fixed::{
	traits::{Fixed, FixedSigned, ToFixed},
	types::extra::{U4, U8},
	FixedI16, FixedI32,
};
pub use fixed_macro::types::I12F4 as fixscalar;
use fixed_sqrt::FixedSqrt;
use gfx::glam::Vec2;
use std::ops::{Add, AddAssign, Div, Mul, Sub, SubAssign};

const LASER_VEL: FixScalar = fixscalar!(31.0);

/// Scalar type used for the game objects' kinetics.
///
/// A signed fixed point number with 11 integer bits and 4 fraction bits,
/// meaning a [-2048,2027] integer range and 16 subpixels.
pub type FixScalar = FixedI16<U4>;

/// Scalar type with (about) double the precision of [`FixScalar`].
///
/// Used for computations that risk overflowing before the end.
type DfixScalar = FixedI32<U8>;

/// Position or velocity of a game object.
///
/// Use fixed points numbers only to handle sub-pixel kinetics. The
/// "canonical" position is the rounding of that number to the nearest
/// integer. It is coherent the object hitbox's position, and used with
/// offsets to define the location at which to draw the associated graphics.
#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct FixVecGeneric<F: Fixed + FixedSigned> {
	pub x: F,
	pub y: F,
}

pub type FixVec = FixVecGeneric<FixScalar>;
type DfixVec = FixVecGeneric<DfixScalar>;

// TODO: A lot of methods could be made const, but it'll need
// feature(const_fn_trait_bound).
impl<F: Fixed + FixedSigned> FixVecGeneric<F> {
	/// Zero vector.
	pub const ZERO: FixVecGeneric<F> = FixVecGeneric::<F> {
		x: F::ZERO,
		y: F::ZERO,
	};

	/// Constructs a vector from [`FixScalar`] components.
	#[inline]
	pub fn new(x: F, y: F) -> FixVecGeneric<F> {
		FixVecGeneric::<F> { x, y }
	}

	/// Constructs a vector from casted components.
	///
	/// The arguments can be other [`FixScalar`]s, integers or floating
	/// points. Floating point numbers will be rounded to the nearest
	/// integer, with ties rounding to even. See [`FixScalar::from_num`].
	///
	/// # Panics
	///
	/// Same as [`FixScalar::from_num`].
	pub fn from_num<X: ToFixed, Y: ToFixed>(
		x: X,
		y: Y,
	) -> FixVecGeneric<F> {
		FixVecGeneric::<F> {
			x: F::from_num(x),
			y: F::from_num(y),
		}
	}

	/// Round to the nearest pixel.
	///
	/// This zeroes out the sub-pixel (fraction) part and rounds to
	/// the nearest integer, giving the canonical position of the
	/// game object.
	pub fn pixel_pos(self) -> FixVecGeneric<F> {
		FixVecGeneric::<F> {
			x: self.x.round_ties_to_even(),
			y: self.y.round_ties_to_even(),
		}
	}

	/// Cast to floating point for OpenGL.
	///
	/// This first rounds to the nearest pixel.
	// TODO: Account, somewhere, for a possible pixel-offset.
	pub fn gl_pos(self) -> Vec2 {
		Vec2::new(
			self.x.round_ties_to_even().to_num::<f32>(),
			self.y.round_ties_to_even().to_num::<f32>(),
		)
	}

	pub fn angle_between(self, other: FixVecGeneric<F>) -> f32 {
		// TODO: uuh
		use std::f32::consts::FRAC_PI_2;
		let dx = self.x - other.x;
		let dy = self.y - other.y;
		f32::atan2(dy.to_num(), dx.to_num()) + FRAC_PI_2
	}

	fn length2(self) -> F {
		self.x * self.x + self.y * self.y
	}
}

impl FixVec {
	pub fn div_euclid_int(self, rhs: i16) -> FixVec {
		FixVec {
			x: self.x.div_euclid_int(rhs),
			y: self.y.div_euclid_int(rhs),
		}
	}
}

impl FixVec {
	#[deprecated = "put that somewhere else"]
	pub fn dvel(self, to: FixVec, n: i16) -> FixVec {
		FixVec {
			x: FixScalar::saturating_sub(to.x, self.x)
				.unwrapped_div_int(n),
			y: FixScalar::saturating_sub(to.y, self.y)
				.unwrapped_div_int(n),
		}
	}
}

impl FixVec {
	fn from_dfixvec(dfixvec: DfixVec) -> FixVec {
		FixVec {
			x: FixScalar::from_num(dfixvec.x),
			y: FixScalar::from_num(dfixvec.y),
		}
	}
}

impl DfixVec {
	fn from_fixvec(fixvec: FixVec) -> DfixVec {
		DfixVec {
			x: DfixScalar::from_num(fixvec.x),
			y: DfixScalar::from_num(fixvec.y),
		}
	}
}

/// Creates a [`FixedVec`] from integer or floating point literals.
///
/// Can be used in const contexts.
#[macro_export]
macro_rules! fixvec {
	($x:literal, $y:literal) => {
		$crate::fixed_kinetics::FixVec {
			x: $crate::fixed_kinetics::fixscalar!($x),
			y: $crate::fixed_kinetics::fixscalar!($y),
		}
	};
}

impl<F: Fixed + FixedSigned> Add<FixVecGeneric<F>> for FixVecGeneric<F> {
	type Output = FixVecGeneric<F>;
	fn add(self, rhs: FixVecGeneric<F>) -> FixVecGeneric<F> {
		FixVecGeneric::<F> {
			x: self.x + rhs.x,
			y: self.y + rhs.y,
		}
	}
}

impl<F: Fixed + FixedSigned> AddAssign<FixVecGeneric<F>> for FixVecGeneric<F> {
	fn add_assign(&mut self, other: FixVecGeneric<F>) {
		*self = *self + other;
	}
}

impl<F: Fixed + FixedSigned> Sub<FixVecGeneric<F>> for FixVecGeneric<F> {
	type Output = FixVecGeneric<F>;
	fn sub(self, rhs: FixVecGeneric<F>) -> FixVecGeneric<F> {
		FixVecGeneric::<F> {
			x: self.x - rhs.x,
			y: self.y - rhs.y,
		}
	}
}

impl<F: Fixed + FixedSigned> SubAssign<FixVecGeneric<F>> for FixVecGeneric<F> {
	fn sub_assign(&mut self, other: FixVecGeneric<F>) {
		*self = *self - other;
	}
}

impl<F: Fixed + FixedSigned> Mul<F> for FixVecGeneric<F> {
	type Output = FixVecGeneric<F>;
	fn mul(self, rhs: F) -> FixVecGeneric<F> {
		FixVecGeneric::<F> {
			x: self.x * rhs,
			y: self.y * rhs,
		}
	}
}

impl<F: Fixed + FixedSigned> Div<F> for FixVecGeneric<F> {
	type Output = FixVecGeneric<F>;
	fn div(self, rhs: F) -> FixVecGeneric<F> {
		FixVecGeneric::<F> {
			x: self.x / rhs,
			y: self.y / rhs,
		}
	}
}

/// Returns the player character velocity from the [`Controller`] bits.
pub fn player_ctrl_vel(ctrl: Controller) -> FixVec {
	const VEL: FixScalar = fixscalar!(4.8);
	// 1/√2
	const DIAG_FACTOR: FixScalar =
		fixscalar!(0.7071067811865475244008443621048490392848);
	const ZERO: FixScalar = FixScalar::ZERO;
	let diag_vel = DIAG_FACTOR * VEL;

	match ctrl.0 & Controller::DIRECTIONS {
		0 => FixVec::new(ZERO, ZERO),
		Controller::RIGHT => FixVec::new(VEL, ZERO),
		Controller::UP => FixVec::new(ZERO, VEL),
		Controller::LEFT => FixVec::new(-VEL, ZERO),
		Controller::DOWN => FixVec::new(ZERO, -VEL),
		Controller::RIGHT_UP => FixVec::new(diag_vel, diag_vel),
		Controller::RIGHT_DOWN => FixVec::new(diag_vel, -diag_vel),
		Controller::UP_LEFT => FixVec::new(-diag_vel, diag_vel),
		Controller::LEFT_DOWN => FixVec::new(-diag_vel, -diag_vel),
		_ => unreachable!("illegal direction inputs"),
	}
}

/// Compute the velocity of a laser head.
///
/// Depends on the current position of the head, the position/velocity of
/// the laser's origin, and the position/velocity of the target.
pub fn laser_head_vel(
	h_pos: FixVec,
	o_pos: FixVec,
	o_vel: FixVec,
	t_pos: FixVec,
	t_vel: FixVec,
) -> FixVec {
	// Obtained by deriving the kinetic equation
	// `h(t) = o(t) + V × t × n(t)`
	// w.r.t. time `t`, with `h` the laser's head position, `o` its
	// origin, `V` a constant growth velocity, and `n` the normal toward
	// its target.

	// Due to numerical errors, the laser head can fly past the target
	// or creep away when it's already on it. To prevent that, the head is
	// snapped to the target exactly if the distance from it is less than V.

	let h_pos = DfixVec::from_fixvec(h_pos);
	let o_pos = DfixVec::from_fixvec(o_pos);
	let o_vel = DfixVec::from_fixvec(o_vel);
	let t_pos = DfixVec::from_fixvec(t_pos);
	let t_vel = DfixVec::from_fixvec(t_vel);
	let laser_vel = DfixScalar::from_num(LASER_VEL);
	let laser_vel2 = laser_vel * laser_vel;

	let r_oh = h_pos - o_pos;
	let d_oh = (r_oh.x * r_oh.x + r_oh.y * r_oh.y).sqrt();

	let r_ht = t_pos - h_pos;
	let d2_ht = r_ht.length2();
	if d2_ht <= laser_vel2 {
		return FixVec::from_dfixvec(r_ht);
	}
	let d_ht = d2_ht.sqrt();
	let laser_vel = DfixScalar::min(DfixScalar::from_num(LASER_VEL), d_ht);

	let r_ot = t_pos - o_pos;
	let d2_ot = r_ot.length2();
	if d2_ot <= laser_vel2 && d_oh.is_zero() {
		return FixVec::from_dfixvec(r_ht);
	}
	let d_ot = d2_ot.sqrt();

	let normal = r_ot / d_ot;

	let v_ot = t_vel - o_vel;
	let scal = v_ot.x * r_ot.x + v_ot.y * r_ot.y;
	let h_vel = o_vel + normal * laser_vel + v_ot * (d_oh / d_ot)
		- (normal * (d_oh * scal)) / d2_ot;
	FixVec::from_dfixvec(h_vel)
}
