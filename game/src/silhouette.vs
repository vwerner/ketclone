//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

#version 330
layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;

uniform mat4 model_to_camera;
uniform mat3 normal_model_to_camera;

layout(std140) uniform camera_to_clip_block {
	mat4 camera_to_clip;
};

layout(std140) uniform outline_block {
	float outline_width;
};

void main() {
	vec4 camera_pos = model_to_camera * vec4(vertex_position, 1.0);
	vec3 camera_normal = normalize(
    		normal_model_to_camera * vertex_normal);
	vec4 camera_offset = outline_width * vec4(camera_normal, 0.0);
	gl_Position = camera_to_clip * (camera_pos + camera_offset);
}
