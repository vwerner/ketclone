//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

//! Just make game.

// #![feature(const_fn_trait_bound)]

mod board;
mod controller;
mod kinetics;

use board::{
	All, Board, BoardSection, Enemies, GfxData, Hitbox, Orrery, Player,
	Shots,
};
use controller::Controller;
use gfx::gl::{self, types::*};
use gfx::glam::{const_vec2 as vec2, Mat4, Quat, Vec3, Vec4};
use gfx::scene::{Camera, Outline, PointLight, Projection, VIEW_ANGLE};
use gfx::{
	self, uniform_buffers, MaterialUbo, ModelStore, ModelWorldTransform,
	RenderQueue, RenderStateStore, SectionArg, TexQuadRenderer, Ubo,
	WindingOrder,
};
use itertools::izip;
use kinetics::Vec2;
use sdl2::video::{GLContext, SwapInterval, Window};
use sdl2::{Sdl, VideoSubsystem};
use std::cmp::Ordering;
use std::f32::consts::FRAC_PI_2;
use std::iter;
use std::ptr;
use std::thread;
use std::time::{Duration, Instant};

/// Width of the game window.
const WINDOW_WIDTH: usize = 342;
/// Height of the game window.
const WINDOW_HEIGHT: usize = 456;
/// Aspect ratio of the game window.
const ASPECT_RATIO: f32 = (WINDOW_WIDTH as f64 / WINDOW_HEIGHT as f64) as f32;
/// Width of the game window, float.
const WINDOW_WIDTH_F: f32 = WINDOW_WIDTH as f32;
/// Height of the game window, float.
const WINDOW_HEIGHT_F: f32 = WINDOW_HEIGHT as f32;

// Must use a plain array and not a glam::Vec4. Vec4 is a SIMD type with
// its own alignment requirement, and it'll mess with the alignment required
// on OpenGL's uniformn buffer.
#[repr(transparent)]
#[derive(Clone, Copy, Debug)]
pub struct OverlayColor([f32; 4]);

uniform_buffers! { mod my_ubos {
	pub struct SceneBlocks {
		camera_to_clip_block: Mat4,
		point_light_block: PointLight,
		outline_block: Outline,
	}

	pub struct Materials {
		overlay_color_mat: [OverlayColor; 3],
	}
}}
type SceneBlocks = my_ubos::SceneBlocks;
type Materials = my_ubos::Materials;

fn board_cameras(center_y: f32, radius: f32) -> (Camera, Camera) {
	let (s, c) = VIEW_ANGLE.sin_cos();
	let eye = Vec3::new(0.0, -radius * s, radius * c);
	let center = Vec3::new(0.0, center_y, 0.0);
	let meshes = Camera { eye, center };

	let eye = Vec3::new(0.0, 0.0, radius * c);
	let center = Vec3::ZERO;
	let quads = Camera { eye, center };

	(meshes, quads)
}

/// Player animation.
fn player_anim(player_vel: Vec2, game_tick: usize, rect_idx: &mut usize) {
	const MIN_IDX: usize = 0;
	const BASE_IDX: usize = 3;
	const MAX_IDX: usize = 6;

	if game_tick % 4 != 0 {
		return;
	}

	match player_vel.x.partial_cmp(&0.0) {
		Some(Ordering::Less) => {
			*rect_idx = (rect_idx.saturating_sub(1)).max(MIN_IDX)
		}
		Some(Ordering::Greater) => {
			*rect_idx = (*rect_idx + 1).min(MAX_IDX)
		}
		Some(Ordering::Equal) => match (*rect_idx).cmp(&BASE_IDX) {
			Ordering::Less => *rect_idx += 1,
			Ordering::Greater => *rect_idx -= 1,
			Ordering::Equal => (),
		},
		_ => unreachable!(),
	}
}

/// Initialize the SDL and OpenGL contexts and the game window. OpenGL
/// functions are callable only after this function.
fn init() -> (Sdl, VideoSubsystem, Window, GLContext) {
	let sdl_ctx = sdl2::init().unwrap();
	let sdl_video = sdl_ctx.video().unwrap();
	let sdl_gl = sdl_video.gl_attr();
	sdl_gl.set_context_profile(sdl2::video::GLProfile::Core);
	sdl_gl.set_context_version(3, 3);

	let window = sdl_video
		.window("ketsui two", WINDOW_WIDTH as u32, WINDOW_HEIGHT as u32)
		.opengl()
		.build()
		.unwrap();
	let gl_ctx = window.gl_create_context().unwrap();
	gl::load_with(|name| sdl_video.gl_get_proc_address(name) as *const _);
	assert_eq!(sdl_gl.context_profile(), sdl2::video::GLProfile::Core);
	assert_eq!(sdl_gl.context_version(), (3, 3));
	// By default, the swap interval is vsync'd and vsync sucks.
	sdl_video
		.gl_set_swap_interval(SwapInterval::Immediate)
		.unwrap();

	// Initialize some OpenGL parameters.
	unsafe {
		gl::ClearColor(0.6, 0.2, 0.8, 1.0);
		gl::Enable(gl::DEPTH_TEST);
		gl::DepthFunc(gl::LEQUAL);
		gl::Enable(gl::BLEND);
		gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
		gl::Enable(gl::CULL_FACE);
		gl::FrontFace(gl::CCW);
		gl::CullFace(gl::BACK);
	}

	(sdl_ctx, sdl_video, window, gl_ctx)
}

fn main() {
	let (sdl_ctx, _sdl_video, window, _gl_ctx) = init();

	// Prepare graphics.
	let models = ModelStore::with_model_files(&[
		"./assets/models/monkey.model",
		"./assets/models/grid16.model",
	]);
	let render_states = RenderStateStore::new(
		&[
			include_bytes!("fragment_lighting.vs"),
			include_bytes!("silhouette.vs"),
		],
		&[
			include_bytes!("fragment_lighting.fs"),
			include_bytes!("silhouette.fs"),
			include_bytes!("tiled.fs"),
		],
		&[(0, 0), (1, 1), (0, 2)],
		&[
			"./assets/textures/brown.texture",
			"./assets/textures/plaid.texture",
		],
	);
	let mut ubo = SceneBlocks::new(
		Mat4::IDENTITY,
		PointLight::default(),
		Outline(3.0),
	);
	let mut mat_ubo = Materials::new([
		OverlayColor([1.0, 1.0, 1.0, 1.0]),
		OverlayColor([0.6, 1.0, 0.9, 1.0]),
		OverlayColor([1.0, 0.5, 0.7, 1.0]),
	]);

	// TODO: Block bindings must be done after when the UBOs are
	// bound. Find a cleaner way to set them up.
	unsafe {
		gl::BindBuffer(gl::UNIFORM_BUFFER, mat_ubo.gl_name());
		render_states.mat_binding::<Materials, OverlayColor>(0);
		gl::BindBuffer(gl::UNIFORM_BUFFER, ubo.gl_name());
		// TODO: looking at this now, it isn't really obvious what
		// this function does.
		render_states.block_binding::<SceneBlocks, Mat4>(0);
		render_states.block_binding::<SceneBlocks, PointLight>(0);
		render_states.block_binding::<SceneBlocks, Mat4>(1);
		render_states.block_binding::<SceneBlocks, Outline>(1);
	}

	let mut render_queue = RenderQueue::with_capacity(
		2,
		&render_states,
		&models,
		&mut ubo,
		&mut mat_ubo,
	);
	unsafe {
		render_queue.push_node_key(
			ModelWorldTransform {
				pos: Vec3::new(0.0, 0.0, 30.0),
				scale: Vec3::new(
					1.0 * WINDOW_HEIGHT_F,
					1.0 * WINDOW_HEIGHT_F,
					1.0,
				),
				quat: Quat::IDENTITY,
			},
			1,
			render_queue.key_for(WindingOrder::CCW, 2, 1),
			render_queue.mat_key_for([None]),
		);
	}
	let monke_game_pos = Vec2::new(0.0, 0.25 * WINDOW_HEIGHT_F);
	let monke_base_quat = Quat::from_rotation_x(FRAC_PI_2);
	let monke_transform = ModelWorldTransform {
		pos: monke_game_pos.extend(0.2 * WINDOW_HEIGHT_F),
		scale: Vec3::new(
			0.2 * WINDOW_WIDTH_F,
			0.2 * WINDOW_WIDTH_F,
			0.2 * WINDOW_WIDTH_F,
		),
		quat: monke_base_quat,
	};
	let mon_key = render_queue.key_for(WindingOrder::CCW, 0, 0);
	let mat_mon_key = render_queue.mat_key_for([Some(0)]);
	// render_queue.push_node(monke_transform, 0, WindingOrder::CCW, 0, 0);
	// render_queue.push_node(monke_transform, 0, WindingOrder::CW, 1, 0);

	let (camera_3d, camera_quad) =
		board_cameras(64.0, 0.3 * WINDOW_HEIGHT_F);

	let camera_to_clip = Projection::new(Mat4::orthographic_rh_gl(
		-0.5 * WINDOW_WIDTH_F,
		0.5 * WINDOW_WIDTH_F,
		-0.5 * WINDOW_HEIGHT_F,
		0.5 * WINDOW_HEIGHT_F,
		-1.0 * WINDOW_HEIGHT_F,
		1.0 * WINDOW_HEIGHT_F,
	));

	// Update the camera to clip uniform.
	// Again, assumes the ubo is still bound.
	*render_queue.ubo().get_mut_of::<Mat4>() = camera_to_clip.mat;

	let point_light = PointLight {
		pos: camera_3d.look_at()
			* Vec4::new(
				0.25 * WINDOW_HEIGHT_F,
				0.45 * WINDOW_WIDTH_F,
				0.9 * WINDOW_HEIGHT_F,
				1.0,
			),
		color: Vec4::new(1.0, 1.0, 1.0, 1.0),
	};
	*render_queue.ubo().get_mut_of::<PointLight>() = point_light;

	// 0: player, 1..4: orrery, 5..: shots.

	let mut board = Box::new(Board::new());
	let player_init_pos = Vec2::new(-128.0, -128.0);
	// Kinda weird to use velocities for the offsets here...
	const ORRERY_INIT_OFFSET: [Vec2; 4] = [
		vec2!([48.0, 0.0]),
		vec2!([32.0, 16.0]),
		vec2!([-32.0, 16.0]),
		vec2!([-48.0, 0.0]),
	];

	board.push::<Player>(
		player_init_pos,
		Vec2::ZERO,
		GfxData::Quad(0, 3),
		Hitbox::new(2, 2),
	);
	for (off, rect_idx) in izip!(ORRERY_INIT_OFFSET.iter(), 7..11) {
		board.push::<Orrery>(
			player_init_pos + *off,
			Vec2::ZERO,
			GfxData::Quad(0, rect_idx),
			Hitbox::NONE,
		);
	}
	board.push::<Enemies>(
		monke_game_pos,
		Vec2::ZERO,
		GfxData::model(
			monke_transform,
			0,
			mon_key,
			mat_mon_key,
			&mut render_queue,
		),
		Hitbox::new(128, 20),
	);

	let atlases = ["assets/textures/player.texture"];
	let sections = [SectionArg {
		capacity: Board::MAX_OBJECTS,
		z: 0.45 * WINDOW_HEIGHT_F,
		texture_idx: 0,
		overlay_color: Vec4::new(1.0, 1.0, 1.0, 1.0),
	}];
	let mut quad_renderer = TexQuadRenderer::new(&atlases, &sections);
	let image_to_clip = camera_to_clip.mat * camera_quad.look_at();

	let mut ctrl = Controller::new();
	const SHOT_COUNTER_INCREMENT: u8 = 1 << 6;
	let mut shot_counter: u8 = 0;
	const FANOUT_COUNTER_MAX: u8 = 27;
	let mut fanout_counter: u8 = FANOUT_COUNTER_MAX;
	const ORRERY_END_OFFSET: [Vec2; 4] = [
		vec2!([72.0, 0.0]),
		vec2!([48.0, 8.0]),
		vec2!([-48.0, 8.0]),
		vec2!([-72.0, 0.0]),
	];
	let orrery_fanout_dvel: [Vec2; 4] = [
		(ORRERY_END_OFFSET[0] - ORRERY_INIT_OFFSET[0])
			/ FANOUT_COUNTER_MAX as f32,
		(ORRERY_END_OFFSET[1] - ORRERY_INIT_OFFSET[1])
			/ FANOUT_COUNTER_MAX as f32,
		(ORRERY_END_OFFSET[2] - ORRERY_INIT_OFFSET[2])
			/ FANOUT_COUNTER_MAX as f32,
		(ORRERY_END_OFFSET[3] - ORRERY_INIT_OFFSET[3])
			/ FANOUT_COUNTER_MAX as f32,
	];
	let mut laser_heads: [Option<usize>; 4] = [None; 4];
	let mut laser_uv_offsets = [0.0; 4];

	let mut sdl_event_pump = sdl_ctx.event_pump().unwrap();
	let mut game_tick = 0;
	'game_loop: loop {
		let begin = Instant::now();
		// Inputs.
		ctrl.handle_events(sdl_event_pump.poll_iter());
		if ctrl.signals_exit() {
			break 'game_loop;
		}

		// Player movement and animation.
		let player_vel = kinetics::player_ctrl_vel(ctrl);
		for vel in board.velocities_mut::<Player>().iter_mut() {
			*vel = player_vel
		}
		for vel in board.velocities_mut::<Orrery>().iter_mut() {
			*vel = player_vel
		}
		let player_rectidx: &mut usize = match board
			.gfx_data_mut::<Player>()[0]
		{
			GfxData::Quad(_, ref mut rectidx) => rectidx,
			_ => unreachable!("player gfx data isn't quad data"),
		};
		player_anim(player_vel, game_tick, player_rectidx);
		let it = izip!(
			board.velocities_mut::<Orrery>().iter_mut(),
			orrery_fanout_dvel.iter()
		);
		#[allow(clippy::collapsible_else_if)]
		if (ctrl.0 & Controller::SHOT) != 0 {
			if fanout_counter > 0 {
				fanout_counter -= 1;
				for (vel, dvel) in it {
					*vel += *dvel;
				}
			}
		} else {
			if fanout_counter < FANOUT_COUNTER_MAX {
				fanout_counter += 1;
				for (vel, dvel) in it {
					*vel -= *dvel;
				}
			}
		}

		board.kinetics();
		// Dirty.
		for gfx in board.gfx_data_mut::<Enemies>().iter() {
			if let GfxData::Model(queue_idx) = gfx {
				render_queue
					.set_mat_key(*queue_idx, mat_mon_key)
			}
		}
		let blink_key = render_queue.mat_key_for([Some(1)]);
		board.test_hits(|gfx| match gfx {
			GfxData::Model(queue_idx) => {
				if game_tick % 5 == 0 {
					render_queue.set_mat_key(
						queue_idx, blink_key,
					);
				}
			}
			GfxData::Quad(_, _) => (),
			GfxData::None => (),
		});
		board.kill_out();

		// To pass the borrow check later, copy the current positions.
		let player_pos = board.positions::<Player>()[0];
		let mut orrery_pos = [Vec2::ZERO; Orrery::LEN];
		let mut orrery_vel = [Vec2::ZERO; Orrery::LEN];
		unsafe {
			let src = &board.positions::<Orrery>();
			ptr::copy(
				&src[0] as *const _,
				&mut orrery_pos as *mut _,
				src.len(),
			);
			let src = &board.velocities::<Orrery>();
			ptr::copy(
				&src[0] as *const _,
				&mut orrery_vel as *mut _,
				src.len(),
			);
		}

		// Player shot.
		if ctrl.0 & Controller::RAPIDFIRE != 0 && fanout_counter > 0 {
			shot_counter = u8::wrapping_add(
				shot_counter,
				SHOT_COUNTER_INCREMENT,
			);
		} else {
			shot_counter = 0;
		}
		// Spawn new shots.
		if shot_counter == SHOT_COUNTER_INCREMENT {
			const SHOT_VEL: Vec2 = vec2!([0.0, 20.0]);
			const PLAYER_SHOT_OFFSETS: [Vec2; 3] = [
				vec2!([0.0, 32.0]),
				vec2!([10.0, 28.0]),
				vec2!([-10.0, 28.0]),
			];
			const ORRERY_SHOT_OFFSET: Vec2 = vec2!([0.0, 20.0]);
			let it = izip!(
				iter::repeat(player_pos),
				PLAYER_SHOT_OFFSETS.iter()
			)
			.map(|(pos, off)| pos + *off)
			.chain(orrery_pos
				.iter()
				.map(|pos| *pos + ORRERY_SHOT_OFFSET));
			for origin in it {
				board.push::<Shots>(
					origin,
					SHOT_VEL,
					GfxData::Quad(0, 11),
					Hitbox::new(24, 48),
				);
			}
		}

		// Lasers.
		if fanout_counter == 0 {
			let it = izip!(
				laser_heads.iter_mut(),
				orrery_pos.iter(),
				orrery_vel.iter()
			);
			for (head, o_pos, o_vel) in it {
				// board::push MUST be lazily evaluated.
				let h_idx = *head.get_or_insert_with(|| {
					board.push::<Shots>(
						*o_pos,
						Vec2::ZERO,
						GfxData::None,
						Hitbox::new(16, 16),
					)
				});
				// Dirty: set alive back since the collision will kill.
				board.alive_mut::<All>()[h_idx] = true;
				let h_pos = board.positions::<All>()[h_idx];
				let t_pos = monke_game_pos;
				let t_vel = Vec2::ZERO;

				let h_vel = kinetics::laser_head_vel_scalar(
					h_pos, *o_pos, *o_vel, t_pos, t_vel,
				);
				board.velocities_mut::<All>()[h_idx] = h_vel;
			}
		} else {
			for head in laser_heads.iter_mut() {
				if let Some(i) = *head {
					board.alive_mut::<All>()[i] = false;
					*head = None;
				}
			}
		}

		let it = izip!(
			laser_heads.iter().flatten().map(|i| (
				board.positions::<All>()[*i],
				board.velocities::<All>()[*i],
			)),
			orrery_pos.iter(),
			orrery_vel.iter(),
			laser_uv_offsets.iter_mut(),
		);
		for ((h_pos, _h_vel), o_pos, o_vel, uv_off) in it {
			// There's probably some redudant computations.
			let midpoint = 0.5 * (*o_pos + h_pos);
			let midpoint_vel = 0.5 * (*o_vel);
			let r_oh = h_pos - *o_pos;
			let length = r_oh.length();
			let angle = kinetics::angle_between(*o_pos, h_pos);
			// TODO: Pen n paper and figure that out.
			let uv_correction = if midpoint_vel.length() < 1.0E-4 {
				0.0
			} else {
				gfx::glam::Vec2::dot(midpoint_vel, r_oh)
					/ midpoint_vel.length()
			};
			*uv_off = (*uv_off + 4.0 + uv_correction) % 256.0;
			quad_renderer.push_laser_to_section(
				0, 12, midpoint, length, *uv_off, angle,
			);
		}

		// board.visualize_hitboxes(&mut quad_renderer);
		board.send_gfx_data(&mut render_queue, &mut quad_renderer);

		// Rotate the model to follow the player.
		let angle = kinetics::angle_between(
			board.positions::<Player>()[0],
			monke_game_pos,
		);
		let face_quat = Quat::from_rotation_y(angle);
		render_queue.set_transform_with(1, |t| {
			t.quat = monke_base_quat * face_quat;
		});
		/* render_queue.set_transform_with(1, |t| {
			t.quat = monke_base_quat * face_quat;
		}); */

		// Drawing.
		unsafe {
			gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

			gl::Viewport(
				0,
				0,
				WINDOW_WIDTH as GLsizei,
				WINDOW_HEIGHT as GLsizei,
			);

			let look_at = camera_3d.look_at();

			render_queue.draw_nodes(&look_at);
			quad_renderer.draw(image_to_clip);
			quad_renderer.clear();
		}
		window.gl_swap_window();

		const FRAME_DURATION: Duration =
			Duration::from_nanos(1_000_000_000 / 60);
		if let Some(d) = FRAME_DURATION.checked_sub(begin.elapsed()) {
			thread::sleep(d);
		} else {
			eprintln!("Couldn't maintain framerate!")
		}

		game_tick += 1;
	}

	gfx::glerr!();
	// Cleanup.
	// The stores already do their own thing when dropped.
}
