//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

//! This module defines the [`Controller`] struct, an intermediary
//! representation of player inputs from SDL inputs.

use sdl2::event::{Event, EventPollIterator};
use sdl2::keyboard::Keycode;

/// Underlying type of [`ControlBits`].
///
/// [`ControlBits`]: ./struct.ControlBits.html
type ControlBits = u16;

/// Representation of the game's binary player inputs.
///
/// A bitfield. Individual bits may represent the on/off state of a digital
/// control (direction controls), or some intermediate, less explicit state
/// (direction carry bits).
///
/// All values of the bitmask may not be allowed: for instance, in this
/// representation, simultaneous left/right and up/down inputs are illegal.
/// The [`Controller::handle_events`] function is responsible for producing
// only legal control bits, using the aformentioned intermediate bits
/// if necessary.
#[derive(Clone, Copy, Debug)]
pub struct Controller(pub ControlBits);

#[rustfmt::skip]
impl Controller {
	pub const RIGHT:     ControlBits = 0b1_0000_0000;
	pub const UP:        ControlBits = 0b0_1000_0000;
	pub const LEFT:      ControlBits = 0b0_0100_0000;
	pub const DOWN:      ControlBits = 0b0_0010_0000;
	pub const SHOT:      ControlBits = 0b0_0001_0000;
	pub const RAPIDFIRE: ControlBits = 0b0_0000_1000;
	/// Carry bit used to handle simultaneous left/right button input
	pub const CARRY_X:   ControlBits = 0b0_0000_0100;
	/// Carry bit used to handle simultaneous up/down button input
	pub const CARRY_Y:   ControlBits = 0b0_0000_0010;
	/// Signal the game loop must exit.
	pub const EXIT:      ControlBits = 0b0_0000_0001;
}

impl Controller {
	/// Distance between the right and x-carry bits.
	const RIGHT_TO_CARRY_SHIFT: usize =
		(Self::RIGHT - Self::CARRY_X).count_ones() as usize;
	/// Distance between the up and y-carry bits.
	const UP_TO_CARRY_SHIFT: usize =
		(Self::UP - Self::CARRY_Y).count_ones() as usize;
	/// Distance between the left and x-carry bits.
	const LEFT_TO_CARRY_SHIFT: usize =
		(Self::LEFT - Self::CARRY_X).count_ones() as usize;
	/// Distance between the down and y-carry bits.
	const DOWN_TO_CARRY_SHIFT: usize =
		(Self::DOWN - Self::CARRY_Y).count_ones() as usize;

	// Having | in match arms is understood as pattern or-ing and not
	// bitwise or-ing, so I need those definitions.
	pub const RIGHT_UP: ControlBits = Self::RIGHT | Self::UP;
	pub const RIGHT_DOWN: ControlBits = Self::RIGHT | Self::DOWN;
	pub const UP_LEFT: ControlBits = Self::UP | Self::LEFT;
	pub const LEFT_DOWN: ControlBits = Self::LEFT | Self::DOWN;

	/// Mask for all directions bits.
	pub const DIRECTIONS: ControlBits =
		Self::RIGHT | Self::UP | Self::LEFT | Self::DOWN;
}

impl Controller {
	/// Returns a controller with no inputs.
	pub const fn new() -> Controller {
		Controller(0)
	}

	/// Handle SDL2 events and update the controller for this game frame.
	///
	/// It is up to this function to generate only legal bits.
	pub fn handle_events(&mut self, iter: EventPollIterator) {
		for evt in iter {
			handle_keyboard_event(&evt, self);
			if let Event::Quit { .. } = evt {
				self.0 |= Self::EXIT;
			}
		}
	}

	/// Returns true if the exit bit is set.
	///
	/// This signals the game loop must break.
	pub fn signals_exit(&self) -> bool {
		self.0 & Self::EXIT != 0
	}
}

/// Handle SDL2 keyboard events and update the [`Controller`].
///
/// It is also up to this function to generate only legal bits.
fn handle_keyboard_event(evt: &Event, ctrl: &mut Controller) {
	macro_rules! e {
		(@keyd $k:ident) => {
			Event::KeyDown { keycode: Some(Keycode::$k), .. }
		};
		(@keyu $k:ident) => {
			Event::KeyUp { keycode: Some(Keycode::$k), .. }
		};
	}
	match evt {
		e!(@keyd Q) => ctrl.0 |= Controller::EXIT,
		e!(@keyd D) => {
			// Set the right bit.
			ctrl.0 |= Controller::RIGHT;
			// Set the carry bit if left was set.
			ctrl.0 |= (ctrl.0 & Controller::LEFT)
				>> Controller::LEFT_TO_CARRY_SHIFT;
			// Clear the left bit.
			ctrl.0 &= !Controller::LEFT;
		}
		e!(@keyd W) => {
			ctrl.0 |= Controller::UP;
			ctrl.0 |= (ctrl.0 & Controller::DOWN)
				>> Controller::DOWN_TO_CARRY_SHIFT;
			ctrl.0 &= !Controller::DOWN;
		}
		e!(@keyd A) => {
			ctrl.0 |= Controller::LEFT;
			ctrl.0 |= (ctrl.0 & Controller::RIGHT)
				>> Controller::RIGHT_TO_CARRY_SHIFT;
			ctrl.0 &= !Controller::RIGHT;
		}
		e!(@keyd S) => {
			ctrl.0 |= Controller::DOWN;
			ctrl.0 |= (ctrl.0 & Controller::UP)
				>> Controller::UP_TO_CARRY_SHIFT;
			ctrl.0 &= !Controller::UP;
		}
		e!(@keyu D) => {
			// Clear the right bit.
			ctrl.0 &= !Controller::RIGHT;
			// Set the left bit if the carry bit was set.
			ctrl.0 |= (ctrl.0 & Controller::CARRY_X)
				<< Controller::LEFT_TO_CARRY_SHIFT;
			// Clear the carry bit.
			ctrl.0 &= !Controller::CARRY_X;
		}
		e!(@keyu W) => {
			ctrl.0 &= !Controller::UP;
			ctrl.0 |= (ctrl.0 & Controller::CARRY_Y)
				<< Controller::DOWN_TO_CARRY_SHIFT;
			ctrl.0 &= !Controller::CARRY_Y;
		}
		e!(@keyu A) => {
			ctrl.0 &= !Controller::LEFT;
			ctrl.0 |= (ctrl.0 & Controller::CARRY_X)
				<< Controller::RIGHT_TO_CARRY_SHIFT;
			ctrl.0 &= !Controller::CARRY_X;
		}
		e!(@keyu S) => {
			ctrl.0 &= !Controller::DOWN;
			ctrl.0 |= (ctrl.0 & Controller::CARRY_Y)
				<< Controller::UP_TO_CARRY_SHIFT;
			ctrl.0 &= !Controller::CARRY_Y;
		}
		e!(@keyd L) => {
			ctrl.0 |= Controller::RAPIDFIRE;
		}
		e!(@keyu L) => {
			ctrl.0 &= !Controller::RAPIDFIRE;
		}
		e!(@keyd J) => {
			ctrl.0 |= Controller::SHOT;
		}
		e!(@keyu J) => {
			ctrl.0 &= !Controller::SHOT;
		}
		_ => {}
	}
}
