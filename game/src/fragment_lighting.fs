//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

#version 330
smooth in vec2 vs_uv;
smooth in vec3 vs_camera_position;
smooth in vec3 vs_camera_normal;
flat in vec4 vs_overlay_color;
out vec4 fs_color;

uniform sampler2D color;

layout(std140) uniform point_light_block {
	vec4 camera_position;
	vec4 color;
} light;

layout(std140) uniform overlay_color_mat {
	vec4 overlay_color;
};

float sharp_gradient(float x) {
	return 0.5 + 0.5*smoothstep(0.2, 0.2125, x);
}

void main() {
	vec3 camera_dir_to_light = normalize(light.camera_position.xyz - vs_camera_position);
	float dotprod = clamp(dot(normalize(vs_camera_normal), camera_dir_to_light),
	                      0.0,
	                      1.0);
	fs_color = overlay_color*vec4(sharp_gradient(dotprod) * light.color.rgb * texture(color, vs_uv).rgb, 1.0);
}
