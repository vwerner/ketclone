//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

#version 330
in vec2 vs_uv;
in vec4 vs_camera_pos;
in vec3 vs_camera_normal;
out vec4 fs_color;

uniform sampler2D color;

void main() {
	// vec4 texture_color = texture(color, vs_uv);
	// vec3 highlight_rgb = vec3(0.0, 0.0, 0.0);
	// float coeff = abs(dot(vs_camera_normal, normalize(-vs_camera_pos.xyz)));
	// coeff = smoothstep(coeff, 0.0, 0.15);
	// // coeff = 1.0;
	// vec3 rgb = (1.0 - coeff)*highlight_rgb + coeff*texture_color.xyz;

	// fs_color = vec4(rgb, texture_color.a);

	fs_color = vec4(texture(color, vs_uv));
}
