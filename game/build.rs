//    Copyright (C) 2021  Werner Verdier
//
//    This file is part of ketclone.
//
//    ketclone is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ketclone is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with ketclone.  If not, see <https://www.gnu.org/licenses/>.

use std::env;

fn main() {
	// Pass the profile (`debug` or `release`) to the `build` cfg
	// variable. With this, attributes like
	// #[cfg(build = "release")]
	// allow conditional compilation of items depending on the build
	// profile, a bit like #define preprocessor macros in C.
	if let Ok(profile) = env::var("PROFILE") {
		println!("cargo:rustc-cfg=build={:?}", profile)
	}
}
